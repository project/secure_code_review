<?php
/**
 * @file
 * Provides samples of functions expecting sanitized input.
 *
 * @author Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

function example_6x_http_header_functions() {
  // drupal_set_header() -- Change the next line but leave this alone
  drupal_set_header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal server error');
  drupal_set_header('HTTP/1.1 503 Service unavailable');
  drupal_set_header('HTTP/1.1 403 Forbidden');
  drupal_set_header('Content-Type: text/plain');
  drupal_set_header('Content-Type: text/javascript; charset=utf-8');
  drupal_set_header('Content-Type: octet/stream');
  drupal_set_header('Content-Disposition: attachment; filename="'. $filename .'"');
  drupal_set_header('Content-Length: '.filesize(file_directory_path().'/'.$filename));

  // Double quotes.
  drupal_set_header($_SERVER["SERVER_PROTOCOL"] . " 500 Internal server error");
  drupal_set_header("HTTP/1.1 503 Service unavailable");
  drupal_set_header("HTTP/1.1 403 Forbidden");
  drupal_set_header("Content-Type: text/plain");
  drupal_set_header("Content-Type: text/javascript; charset=utf-8");
  drupal_set_header("Content-Type: octet/stream");
  drupal_set_header("Content-Disposition: attachment; filename='". $filename ."'");
  drupal_set_header("Content-Length: ".filesize(file_directory_path()."/".$filename));

  // drupal_get_headers() -- Change the next line but leave this alone
  $headers = drupal_get_headers();
  foreach (drupal_get_headers() as $name => $value) {
    //
  }
}

function example_6x_drupal_set_title() {
  // drupal_set_title() -- Change the next line but leave this alone
  drupal_set_title($node->title);
  drupal_set_title($node->title . ($node->title2));
  drupal_set_title(check_plain($node->title));

  drupal_set_title(t("@name's blog and %title", /* smoke */ array('@name' => $account->name, '%title' => 'XXX')));
  drupal_set_title(t("<a href='myblog.com'>@name's blog</a>", array('@name' => $account->name)));
  drupal_set_title(t("<a href='myblog.com'>@name's blog</a>"));
  if (drupal_set_title(t("@name's blog", array('@name' => $account->name)))) {
  }
  drupal_set_title(/* smoke screen */ 'check_plain' . ($node->title) . ($node->title2));
  drupal_set_title($title = NULL);
  drupal_set_title(NULL);
  drupal_set_title('NULL');

  drupal_set_title(/* smoke 1 */ check_plain  (/* smoke 3 */    $node->title/* smoke 4 */ . ($node->title2))/* smoke 5 */);
  drupal_set_title(/* smoke 1 */ check_plain  (/* smoke 3 */    $node->title/* smoke 4 */) . ($node->title2)/* smoke 5 */);
  drupal_set_title(/* smoke 1 */ check_plain/* smoke 2 */ (/* smoke 3 */    $node->title/* smoke 4 */)/* smoke 5 */);
  drupal_set_title(/* smoke screen */ 'check_plain' . ($node->title/* smoke screen */));
}

// Added for GSOC.
function example_6x_drupal_set_message() {
  // drupal_set_message() -- Change the next line but leave this alone
  drupal_set_message($node->message);
  drupal_set_message($node->message . ($node->message2));
  drupal_set_message(check_plain($node->message));

  drupal_set_message(t("@name's blog and %message", /* smoke */ array('@name' => $account->name, '%message' => 'XXX')));
  drupal_set_message(t("<a href='myblog.com'>@name's blog</a>", array('@name' => $account->name)));
  drupal_set_message(t("<a href='myblog.com'>@name's blog</a>"));
  if (drupal_set_message(t("@name's blog", array('@name' => $account->name)))) {
  }
  drupal_set_message(/* smoke screen */ 'check_plain' . ($node->message) . ($node->message2));
  drupal_set_message($message = NULL);
  drupal_set_message(NULL);
  drupal_set_message('NULL');

  drupal_set_message(/* smoke 1 */ check_plain  (/* smoke 3 */    $node->message/* smoke 4 */ . ($node->message2))/* smoke 5 */);
  drupal_set_message(/* smoke 1 */ check_plain  (/* smoke 3 */    $node->message/* smoke 4 */) . ($node->message2)/* smoke 5 */);
  drupal_set_message(/* smoke 1 */ check_plain/* smoke 2 */ (/* smoke 3 */    $node->message/* smoke 4 */)/* smoke 5 */);
  drupal_set_message(/* smoke screen */ 'check_plain' . ($node->message/* smoke screen */));
}

// Added for GSOC.
function example_6x_form_set_error() {
  // form_set_error() -- Change the next line but leave this alone
  form_set_error('author', t('You have to specify a valid author.'));
  form_set_error('author', check_plain($node->message));

  form_set_error('author', t("@name's blog and %message", /* smoke */ array('@name' => $account->name, '%message' => 'XXX')));
  form_set_error('author', t("<a href='myblog.com'>@name's blog</a>", array('@name' => $account->name)));
  form_set_error('author', t("<a href='myblog.com'>@name's blog</a>"));
  if (form_set_error('author', t("@name's blog", array('@name' => $account->name)))) {
  }
}

// Added for GSOC.
function example_6x_confirm_form() {
  // confirm_form() -- Change the next line but leave this alone
  return confirm_form($form, t('Are you sure you want to delete the custom menu %title?', array('%title' => $menu['title'])), 'admin/build/menu-customize/'. $menu['menu_name'], $caption, t('Delete'));
  return confirm_form($form, t('Are you sure you want to delete the custom menu item %item?', array('%item' => $item['link_title'])), 'admin/build/menu-customize/'. $item['menu_name']);
  return confirm_form($form, t('Are you sure you want to reset the item %item to its default values?', array('%item' => $item['link_title'])), 'admin/build/menu-customize/'. $item['menu_name'], t('Any customizations will be lost. This action cannot be undone.'), t('Reset'));
  return confirm_form($form, $question, drupal_get_destination(), $description, t('Set multiple parents'));
  return confirm_form($form, t('Are you sure you want to remove %title from the book hierarchy?', $title), 'node/'. $node->nid, $description, t('Remove'));
}

// Added for GSOC.
function example_6x_db_rewrite_sql() {
  // db_query() -- Change the next line but leave this alone
  $result = db_query(db_rewrite_sql("SELECT n.type, n.title, b.*, ml.* FROM {book} b INNER JOIN {node} n on b.nid = n.nid INNER JOIN {menu_links} ml ON b.mlid = ml.mlid WHERE n.nid IN (". implode(',', $nids) .") AND n.status = 1 ORDER BY ml.weight, ml.link_title"));
  $result = pager_query(db_rewrite_sql("SELECT n.nid, n.sticky, n.created FROM {node} n WHERE n.type = 'blog' AND n.uid = %d AND n.status = 1 ORDER BY n.sticky DESC, n.created DESC"), variable_get('default_nodes_main', 10), 0, NULL, $account->uid);
  $result = pager_query(db_rewrite_sql("SELECT n.nid, n.created FROM {node} n WHERE n.type = 'blog' AND n.status = 1 ORDER BY n.sticky DESC, n.created DESC"), variable_get('default_nodes_main', 10));
  $result = db_query_range(db_rewrite_sql("SELECT n.nid, n.created FROM {node} n  WHERE n.type = 'blog' AND n.uid = %d AND n.status = 1 ORDER BY n.created DESC"), $account->uid, 0, variable_get('feed_default_items', 10));
  $result = db_query_range(db_rewrite_sql("SELECT n.nid, n.created FROM {node} n WHERE n.type = 'blog' AND n.status = 1 ORDER BY n.created DESC"), 0, variable_get('feed_default_items', 10));
  $result = db_query(db_rewrite_sql('SELECT t.tid, COUNT(n.nid) AS c FROM {term_node} t INNER JOIN {node} n ON t.vid = n.vid WHERE n.status = 1 GROUP BY t.tid'));

  // Without db_rewrite_sql().
  $result = db_query("SELECT n.type, n.title, b.*, ml.* FROM {book} b INNER JOIN {node} n on b.nid = n.nid INNER JOIN {menu_links} ml ON b.mlid = ml.mlid WHERE n.nid IN (". implode(',', $nids) .") AND n.status = 1 ORDER BY ml.weight, ml.link_title");
  $result = pager_query("SELECT n.nid, n.sticky, n.created FROM {node} n WHERE n.type = 'blog' AND n.uid = %d AND n.status = 1 ORDER BY n.sticky DESC, n.created DESC", variable_get('default_nodes_main', 10), 0, NULL, $account->uid);
  $result = pager_query("SELECT n.nid, n.created FROM {node} n WHERE n.type = 'blog' AND n.status = 1 ORDER BY n.sticky DESC, n.created DESC", variable_get('default_nodes_main', 10));
  $result = db_query_range("SELECT n.nid, n.created FROM {node} n  WHERE n.type = 'blog' AND n.uid = %d AND n.status = 1 ORDER BY n.created DESC", $account->uid, 0, variable_get('feed_default_items', 10));
  $result = db_query_range("SELECT n.nid, n.created FROM {node} n WHERE n.type = 'blog' AND n.status = 1 ORDER BY n.created DESC", 0, variable_get('feed_default_items', 10));
  $result = db_query_range("SELECT nid FROM {node} WHERE nid > %d ORDER BY nid ASC", $context['sandbox']['current_node'], 0, $limit);
  $result = db_query('SELECT t.tid, COUNT(n.nid) AS c FROM {term_node} t INNER JOIN {node} n ON t.vid = n.vid WHERE n.status = 1 GROUP BY t.tid');

  // Ignore COUNT(*) only queries.
  if (db_result(db_query('SELECT COUNT(*) FROM {node} WHERE tnid = %d', $node->tnid)) == 1) {
  }

  // Ignore non-SELECT queries.
  db_query("UPDATE {node} SET tnid = %d, translate = %d WHERE nid = %d", $tnid, 0, $node->nid);
  db_query("UPDATE {node} SET tnid = %d, translate = %d WHERE nid = %d", $node->tnid, $node->translation['status'], $node->nid);
  db_query('DELETE FROM {node} WHERE nid = %d', $node->nid);
}

// Added for GSOC.
function example_6x_preg_replace() {
  // preg_replace() -- Change the next line but leave this alone
  preg_replace('/$node->message/mse');
  preg_replace('/$node->message/mse');

  $from = /*smoke*/'@comment_node_url()@ie'/*smoke*/;
  $to = "'comment/' . \$comment->cid";
  $temp = preg_replace(/*smoke*/$from/*smoke*/, $to, $temp);

  $string = preg_replace('@[\'"]theme[\'"]@', "'file'", $string); // Could be deleted.
  $string = preg_replace('@[\'"](core|theme)[\'"]@', "'file'", $string); // Could be deleted.

  $pattern = '/(\w+) (\d+), (\d+)/i';
  $replacement = '${1}1,$3';
  echo preg_replace($pattern, $replacement, $string);

  echo preg_replace($missing_pattern, $replacement, $string);
}
