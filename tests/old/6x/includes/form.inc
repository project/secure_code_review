<?php
/**
 * @file
 * Provides samples of forms to review for sanitized input.
 *
 * @author Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
   http://api.drupal.org/api/drupal/developer--topics--forms_api_reference.html/6#markup
   #type  checkbox  checkboxes  date  fieldset  file  password  password_confirm  radio   radios  select  textarea  textfield   weight (13)
   #type  button  image_button  submit  form  hidden  token   markup  item  value (9)

   compare to definitions in system_elements()
   all match

keys from system ==>
Array
(
    [0] => button
    [1] => checkbox
    [2] => checkboxes
    [3] => date
    [4] => fieldset
    [5] => file
    [6] => form
    [7] => hidden
    [8] => image_button
    [9] => item
    [10] => markup
    [11] => password
    [12] => password_confirm
    [13] => radio
    [14] => radios
    [15] => select
    [16] => submit
    [17] => textarea
    [18] => textfield
    [19] => token
    [20] => value
    [21] => weight
)
keys from user ==>
Array
(
    [0] => user_profile_category
    [1] => user_profile_item
)
documented properties
$items ==>
Array
(
    [0] => #access
    [1] => #action
    [2] => #after_build
    [3] => #ahah
    [4] => #attributes
    [5] => #autocomplete_path
    [6] => #button_type
    [7] => #collapsed
    [8] => #collapsible
    [9] => #cols
    [10] => #default_value
    [11] => #delta
    [12] => #description
    [13] => #disabled
    [14] => #element_validate
    [15] => #executes_submit_callback
    [16] => #field_prefix
    [17] => #field_suffix
    [18] => #maxlength
    [19] => #method
    [20] => #multiple
    [21] => #name
    [22] => #options
    [23] => #parents
    [24] => #post_render
    [25] => #pre_render
    [26] => #prefix
    [27] => #process
    [28] => #redirect
    [29] => #required
    [30] => #resizable
    [31] => #return_value
    [32] => #rows
    [33] => #size
    [34] => #src
    [35] => #submit
    [36] => #suffix
    [37] => #theme
    [38] => #title
    [39] => #tree
    [40] => #validate
    [41] => #value
    [42] => #weight
)
elements array properties
$all_properties ==>
Array
(
    [0] => #action
    [1] => #autocomplete_path
    [2] => #button_type
    [3] => #collapsed
    [4] => #collapsible
    [5] => #cols
    [6] => #default_value
    [7] => #delta
    [8] => #element_validate
    [9] => #executes_submit_callback
    [10] => #has_garbage_value
    [11] => #input
    [12] => #maxlength
    [13] => #method
    [14] => #multiple
    [15] => #name
    [16] => #prefix
    [17] => #process
    [18] => #resizable
    [19] => #return_value
    [20] => #rows
    [21] => #size
    [22] => #src
    [23] => #suffix
    [24] => #tree
    [25] => #value
)
$diff1 ==>
Array
(
    [10] => #has_garbage_value
    [11] => #input
)
$diff2 ==>
Array
(
    [0] => #access
    [2] => #after_build
    [3] => #ahah
    [4] => #attributes
    [12] => #description
    [13] => #disabled
    [16] => #field_prefix
    [17] => #field_suffix
    [22] => #options
    [23] => #parents
    [24] => #post_render
    [25] => #pre_render
    [28] => #redirect
    [29] => #required
    [35] => #submit
    [37] => #theme
    [38] => #title
    [40] => #validate
    [42] => #weight
)
 */

/**
 * Form builder.
 *
 * @todo Create a safe version to test for false positives.
 * @todo These forms apply to 7x; remove 7x elements and properties.
 * The form element review is essentially the same between 6x and 7x.
 */
function example_6x_xss_unsafe_form($form_state) {
  $xss_title = '<a href=xss.com>xss vulnerable title</a>';
  $xss_description = '<a href=xss.com>xss vulnerable description</a>';
  $xss_default_value = '<a href=xss.com>xss vulnerable default value</a>';
  $xss_value = '<a href=xss.com>xss vulnerable value</a>';
  $xss_markup = '<a href=xss.com>xss vulnerable markup</a>';
  $xss_option = '<a href=xss.com>xss vulnerable option</a>';
  $xss_options['xss1'] = $xss_option . ' 1';
  $xss_options['xss2'] = $xss_option . ' 2';

  // Form structure.
  $form['actions'] = array(
    '#theme_wrappers' => array('container'),
    '#process' => array('form_process_actions', 'form_process_container'),
    '#weight' => 100,
  );
  $form['container'] = array(
    '#theme_wrappers' => array('container'),
    '#process' => array('form_process_container'),
  );
  $form['fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => $xss_title,
    '#description' => $xss_description,
  );

  // Input elements.
  $form['element_1'] = array(
    '#type' => 'button',
    '#title' => $xss_title,
    '#description' => $xss_description,
    '#value' => $xss_value,
  );
  $form['element_2'] = array(
    '#type' => 'checkbox',
    '#title' => $xss_title,
    '#description' => $xss_description,
    '#default_value' => $xss_default_value,
  );
  $form['element_3'] = array(
    '#type' => 'checkboxes',
    '#title' => $xss_title,
    '#description' => $xss_description,
    '#options' => $xss_options,
  );
  $form['element_4'] = array(
    '#type' => 'date',
    '#title' => $xss_title,
    '#description' => $xss_description,
    '#default_value' => $xss_default_value,
    '#required' => TRUE,
  );
  $form['upload'] = array(
    '#name' => 'files[foo_bar]',
    '#type' => 'file',
    '#title' => $xss_title,
    '#description' => $xss_description,
    '#title_display' => 'invisible',
    '#size' => 22,
//     '#theme_wrappers' => array(),
//     '#weight' => -10,
  );
  $form['element_10'] = array(
    '#type' => 'image_button',
    '#title' => $xss_title,
    '#description' => $xss_description,
    '#value' => $xss_value,
    '#src' => './misc/forum-icons.png',
  );
  $form['element_11'] = array( // @todo This has other properties that determine the link url
    '#type' => 'link',
    '#title' => $xss_title,
    '#description' => $xss_description,
    '#default_value' => $xss_default_value,
    '#href' => $xss_title, // @todo Not doc'd'
  );
  $form['element_15'] = array( // @todo Add other props
    '#type' => 'machine_name',
    '#title' => $xss_title,
    '#description' => $xss_description,
    '#default_value' => $xss_default_value,
  );
  $form['managed_file'] = array(
    '#type' => 'managed_file',
    '#title' => $xss_title,
    '#description' => $xss_description,
    '#title_display' => 'invisible',
    '#size' => 22,
    '#upload_location' => 'public://image_example_images/',
  );

  $form['element_12'] = array(
    '#type' => 'password',
    '#title' => $xss_title,
    '#description' => $xss_description,
    '#default_value' => $xss_default_value,
  );
  $form['element_5'] = array(
    '#type' => 'radio',
    '#title' => $xss_title,
    '#description' => $xss_description,
    '#default_value' => $xss_default_value,
  );
  $form['element_6'] = array(
    '#type' => 'radios',
    '#title' => $xss_title,
    '#description' => $xss_description,
    '#options' => $xss_options,
  );
  $form['element_13'] = array(
    '#type' => 'select',
    '#title' => $xss_title,
    '#description' => $xss_description,
    '#options' => $xss_options,
  );/*
  $form['element_14'] = array(
    '#type' => 'tableselect',
    '#title' => $xss_title,
    '#description' => $xss_description,
    '#header' => $xss_options,
    '#options' => $xss_options,
    '#empty' => t('No comments available'),
  );*/

  $form['element_7'] = array(
    '#type' => 'item',
    '#title' => $xss_title,
    '#description' => $xss_description,
    '#size' => 40,
    '#required' => TRUE,
    '#markup' => $xss_markup,
  );
  $form['element_8'] = array(
    '#type' => 'textfield',
    '#title' => $xss_title,
    '#description' => $xss_description,
    '#size' => 40,
    '#required' => TRUE,
    '#value' => $xss_value,
  );
  $form['element_9'] = array(
    '#type' => 'textarea',
    '#title' => $xss_title,
    '#description' => $xss_description,
    '#size' => 40,
    '#required' => TRUE,
    '#default_value' => $xss_default_value,
  );
  $form['element_82'] = array(
    '#type' => 'text_format',
    '#title' => $xss_title,
    '#description' => $xss_description,
    '#format' => 'full',
    '#size' => 40,
    '#required' => TRUE,
    '#value' => $xss_value,
  );
  $form['element_83'] = array(
    '#type' => 'markup',
    '#markup' => $xss_markup,
  );
  $form['element_84'] = array(
    '#markup' => $xss_markup,
    '#prefix' => '<div class="element-foo">' . $xss_title,
    '#suffix' => $xss_option . '</div>',
  );

  return $form;
}

/**
 * Form builder.
 */
function example_6x_xss_safe_form($form_state) {
  $xss_title = check_plain('<a href=xss.com>xss vulnerable title</a>');
  $xss_description = '<a href=xss.com>xss vulnerable description</a>';
  $xss_default_value = '<a href=xss.com>xss vulnerable default value</a>';
  $xss_value = '<a href=xss.com>xss vulnerable value</a>';
  $xss_markup = '<a href=xss.com>xss vulnerable markup</a>';
  $xss_option = '<a href=xss.com>xss vulnerable option</a>';
  $xss_options['xss1'] = $xss_option . ' 1';
  $xss_options['xss2'] = $xss_option . ' 2';

  // Form structure.
  $form['actions'] = array(
    '#theme_wrappers' => array('container'),
    '#process' => array('form_process_actions', 'form_process_container'),
    '#weight' => 100,
  );
  $form['container'] = array(
    '#theme_wrappers' => array('container'),
    '#process' => array('form_process_container'),
  );
  $form['fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => $xss_title,
    '#description' => check_plain($xss_description),
  );

  // Input elements.
  $form['element_1'] = array(
    '#type' => 'button',
    '#title' => $xss_title,
    '#description' => check_plain($xss_description),
    '#value' => $xss_value,
  );
  $form['element_2'] = array(
    '#type' => 'checkbox',
    '#title' => $xss_title,
    '#description' => check_plain($xss_description),
    '#default_value' => $xss_default_value,
  );
  $form['element_3'] = array(
    '#type' => 'checkboxes',
    '#title' => $xss_title,
    '#description' => check_plain($xss_description),
    '#options' => $xss_options,
  );
  $form['element_4'] = array(
    '#type' => 'date',
    '#title' => $xss_title,
    '#description' => check_plain($xss_description),
    '#default_value' => $xss_default_value,
    '#required' => TRUE,
  );
  $form['upload'] = array(
    '#name' => 'files[foo_bar]',
    '#type' => 'file',
    '#title' => $xss_title,
    '#description' => check_plain($xss_description),
    '#title_display' => 'invisible',
    '#size' => 22,
//     '#theme_wrappers' => array(),
//     '#weight' => -10,
  );
  $form['element_10'] = array(
    '#type' => 'image_button',
    '#title' => $xss_title,
    '#description' => check_plain($xss_description),
    '#value' => $xss_value,
    '#src' => './misc/forum-icons.png',
  );
  $form['element_11'] = array( // @todo This has other properties that determine the link url
    '#type' => 'link',
    '#title' => $xss_title,
    '#description' => check_plain($xss_description),
    '#default_value' => $xss_default_value,
    '#href' => $xss_title, // @todo Not doc'd'
  );
  $form['element_15'] = array( // @todo Add other props
    '#type' => 'machine_name',
    '#title' => $xss_title,
    '#description' => check_plain($xss_description),
    '#default_value' => $xss_default_value,
  );
  $form['managed_file'] = array(
    '#type' => 'managed_file',
    '#title' => $xss_title,
    '#description' => check_plain($xss_description),
    '#title_display' => 'invisible',
    '#size' => 22,
    '#upload_location' => 'public://image_example_images/',
  );

  $form['element_12'] = array(
    '#type' => 'password',
    '#title' => $xss_title,
    '#description' => check_plain($xss_description),
    '#default_value' => $xss_default_value,
  );
  $form['element_5'] = array(
    '#type' => 'radio',
    '#title' => $xss_title,
    '#description' => check_plain($xss_description),
    '#default_value' => $xss_default_value,
  );
  $form['element_6'] = array(
    '#type' => 'radios',
    '#title' => $xss_title,
    '#description' => check_plain($xss_description),
    '#options' => $xss_options,
  );
  $form['element_13'] = array(
    '#type' => 'select',
    '#title' => $xss_title,
    '#description' => check_plain($xss_description),
    '#options' => $xss_options,
  );/*
  $form['element_14'] = array(
    '#type' => 'tableselect',
    '#title' => $xss_title,
    '#description' => check_plain($xss_description),
    '#header' => $xss_options,
    '#options' => $xss_options,
    '#empty' => t('No comments available'),
  );*/

  $form['element_7'] = array(
    '#type' => 'item',
    '#title' => $xss_title,
    '#description' => check_plain($xss_description),
    '#size' => 40,
    '#required' => TRUE,
    '#markup' => $xss_markup,
  );
  $form['element_8'] = array(
    '#type' => 'textfield',
    '#title' => $xss_title,
    '#description' => check_plain($xss_description),
    '#size' => 40,
    '#required' => TRUE,
    '#value' => $xss_value,
  );
  $form['element_9'] = array(
    '#type' => 'textarea',
    '#title' => $xss_title,
    '#description' => check_plain($xss_description),
    '#size' => 40,
    '#required' => TRUE,
    '#default_value' => $xss_default_value,
  );
  $form['element_82'] = array(
    '#type' => 'text_format',
    '#title' => $xss_title,
    '#description' => check_plain($xss_description),
    '#format' => 'full',
    '#size' => 40,
    '#required' => TRUE,
    '#value' => $xss_value,
  );

  return $form;
}

/**
 * Form builder.
 *
 * @see taxonomy_filter_admin_templates_form()
 */
function example_6x_table_1_form($form_state) {
  $templates = _example_6x_templates();
  $vars = array();
  $description = 'Templates provide for different ways of doing something.';
  if (count($templates) < 5) {
    $description .= 'There are five templates available with this module. To enable other templates, install other modules !here.';
    $vars = array('!here' => l('here', 'admin/build/modules'));
  }

  $form = array();

  // Menu template summary.
  $form['templates'] = array(
    '#type' => 'fieldset',
    '#title' => t('Templates'),
    '#description' => t($description, $vars),
  );
  $form['templates']['table'] = array(
    '#theme' => 'example_6x_table_1',
  );
  foreach ($templates as $key => $template) {
    $form['templates']['table'][$key] = array(
      'name' => array('#value' => $template['name']),
      'desc' => array('#value' => $template['desc']),
      'module' => array('#value' => $template['module name']),
      'status' => array('#value' => ucfirst($template['status']), '#class' => $template['status']),
    );
  }

  return $form;
}

/**
 * Returns the themed table.
 *
 * @see theme_taxonomy_filter_templates_table()
 *
 * @param array $form
 *   Form elements.
 */
function theme_example_6x_table_1($form) {
  $xss_title = '<a href=xss.com>xss vulnerable title</a>';
  $xss_description = '<a href=xss.com>xss vulnerable description</a>';

  $rows = array();
  foreach (element_children($form) as $key) {
    $template = &$form[$key];
    $row = array();
    $row[] = check_plain($template['name']['#value']);
    $row[] = filter_xss_admin($template['desc']['#value']);
    $row[] = drupal_render($template['module']);
    $row[] = array('data' => $template['status']['#value'], 'class' => 'status');
    $row[] = array('data' => check_plain($template['status']['#value']), 'class' => 'status');
    $rows[] = array('data' => $row, 'class' => $template['status']['#class']);
  }
  $header = array(t('Name'), t('Description'), t('Module'), t('Status'));
  $header = array(t('Name'), t('Description'), t('Module'), t('Status'), $xss_title);
  $header[] = array(/*smoke*/'data'/*smoke*/ => /*smoke*/$xss_description/*smoke*/);
  $header[] = $xss_description;

  $caption = $xss_title;
//   $output = theme('table', $header, $rows);
//   $output .= theme('table', $header, /*smoke*/ $rows[/*smoke*/'foo'/*smoke*/] /*smoke*/, array(), $caption);
  $output = theme(/*smoke*/'table'/*smoke*/, $header, /*smoke*/ $rows/*smoke*/, array(), $caption);

  return $output;
}

/**
 * Returns list of templates.
 */
function _example_6x_templates() {
  $templates = array();
  for ($i = 0; $i < 5; $i++) {
    $templates[] = array(
      'name' => 'name ' . $i,
      'desc' => 'description ' . $i,
      'module name' => 'module name ' . $i,
      'status' => 'status ' . $i,
    );
  }
  return $templates;
}

/**
 * Returns the themed item list.
 *
 * @param array $form
 *   Form elements.
 */
function theme_example_6x_item_list($form) {
  $xss_item = '<a href=xss.com>xss vulnerable item %d</a>';
  $xss_title = '<a href=xss.com>xss vulnerable title</a>';
  $xss_type = '<a href=xss.com>xss vulnerable type</a>';
  $xss_attribute = '<a href=xss.com>xss vulnerable attribute</a>';

  $attributes = $items = array();
  for ($i = 1; $i < 5; $i++) {
    $items[] = sprintf($xss_item, $i);
  }
  $attributes['class'] = $xss_attribute;
  $output = theme(/*smoke*/'item_list'/*smoke*/, /*smoke*/ $items/*smoke*/, $xss_title, $xss_type, $attributes);

  $xss_title = 'List title';
  $xss_type = 'ul';
  $output = theme(/*smoke*/'item_list'/*smoke*/, /*smoke*/ $items/*smoke*/, $xss_title, $xss_type, $attributes);

  return $output;
}

/**
 * Returns the themed form element.
 *
 * @param array $element
 *   Form element.
 */
function theme_example_6x_form_element($form) {
  $xss_title = '<a href=xss.com>xss vulnerable title</a>';
  $xss_description = '<a href=xss.com>xss vulnerable description</a>';
  $xss_id = '<a href=xss.com>xss vulnerable id</a>';
  $xss_required = '<a href=xss.com>xss vulnerable required</a>';
  $xss_value = '<a href=xss.com>xss vulnerable value</a>';

  $element = array(
    '#id' => $xss_id,
    '#type' => 'foo',
    '#title' => $xss_title,
    '#description' => $xss_description,
    '#required' => $xss_required,
  );

  $output = theme(/*smoke*/'form_element'/*smoke*/, $element, /*smoke*/ $xss_value/*smoke*/);

  return $output;
}
