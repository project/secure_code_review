<?php
/**
 * @file
 * Provides Drush command implementations.
 *
 * Copyright 2009-11 by Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * Implements hook_drush_command().
 */
function secure_code_review_drush_command() {
  $items['secure-code-review-run'] = array(
    'callback' => 'drush_secure_code_review_run', // The default callback name.
    'description' => dt('Run secure code reviews'),
    'arguments' => array(
      'directories' => dt('List of directories in the "old" directory containing modules to review.'),
      'extensions' => dt('List of file extensions used to filter the files to be reviewed.'),
    ),
    'options' => array(
      '--all' => 'Review all modules in the "old" directory.',
    ),
    'drupal dependencies' => array('coder_upgrade', 'secure_code_review'),
    'aliases' => array('scr-run'),
//     'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL, // DRUSH_BOOTSTRAP_DRUSH, // DRUSH_BOOTSTRAP_DRUPAL_ROOT, // DRUSH_BOOTSTRAP_DRUPAL_SITE, // DRUSH_BOOTSTRAP_DRUPAL_CONFIGURATION, //
  );
  return $items;
}

/**
 * Runs the secure code reviews.
 *
 * @todo Implement the $extensions parameter.
 *
 * @param string $directories
 *   (optional) A comma-separated list of directories containing modules to
 *   review. Each directory must exist in the "old" directory.
 * @param string $extensions
 *   (optional) A comma-separated list of file extensions used to filter the
 *   files to be reviewed.
 */
function drush_secure_code_review_run($directories = '', $extensions = 'inc,install,php') {
  // Validate and prepare the directories and file extensions.
  if (!secure_code_review_run_validate($directories, $extensions)) {
    return;
  }

  // Build the item list.
  $old_dir = DRUPAL_ROOT . '/' . secure_code_review_directory_path('old');
  $new_dir = DRUPAL_ROOT . '/' . secure_code_review_directory_path('new');
  $items = array();
  foreach ($directories as $directory) {
    $items[] = array(
      'name' => $directory,
      'old_dir' => $old_dir . $directory,
      'new_dir' => $new_dir . $directory,
    );
  }

  // Set conversion routines.
  $upgrades = secure_code_review_upgrade_info();

  // Backup and set runtime variable.
  $original = variable_get('coder_upgrade_replace_files', FALSE);
  variable_set('coder_upgrade_replace_files', FALSE);

  // Apply conversion routines.
  module_load_include('inc', 'coder_upgrade', 'includes/main');
  $success = coder_upgrade_start($upgrades, $extensions, $items);
  if ($success) {
    drush_print(dt('Module conversion routines were applied.'));
  }
  else {
    drush_set_error('error', dt('Module conversion routines failed to complete.'));
  }

  // Restore runtime variable.
  variable_set('coder_upgrade_replace_files', $original);

  return $success;
}

/**
 * Validates and prepares the command line parameters.
 *
 * @param string $directories
 *   (optional) A comma-separated list of directories containing modules to
 *   review. Each directory must exist in the "old" directory.
 * @param string $extensions
 *   (optional) A comma-separated list of file extensions used to filter the
 *   files to be reviewed.
 *
 * @return boolean
 *   TRUE if parameters are valid, FALSE otherwise.
 */
function secure_code_review_run_validate(&$directories, &$extensions) {
  // Check for any submitted parameters.
  $directories = trim($directories);
  if (empty($directories) && !drush_get_option('all')) {
    drush_set_error('directories', dt('Specify the directories to review or use "--all" to review everything in the "old" directory.'));
    return FALSE;
  }
  $extensions = trim($extensions);
  if (empty($extensions)) {
    drush_set_error('extensions', dt('Specify the extensions of files to be included in the reviews or omit to use default extensions.'));
    return FALSE;
  }

  // Validate the directories.
  module_load_include('inc', 'secure_code_review', 'includes/utility');
  module_load_include('inc', 'coder_upgrade', 'includes/conversion');

  $path = realpath(secure_code_review_directory_path('old', FALSE));
  $all_items = coder_upgrade_scan_directory($path);
  if (empty($all_items)) {
    drush_set_error('directories', dt('Please place modules to be reviewed in @path.', array('@path' => $path)));
    return FALSE;
  }

  if (!empty($directories)) {
    $directories = array_filter(array_map('trim', explode(',', $directories)));
    foreach ($directories as $index => $item) {
      if (!in_array($item, $all_items)) {
        // Prune any input directory not found in old directory.
        unset($directories[$index]);
      }
    }
  }
  else {
    $directories = $all_items;
  }

  if (empty($directories)) {
    drush_set_error('directories', dt('Specify at least one valid directory to review or use "--all" to review everything in the "old" directory.'));
    return FALSE;
  }

  // Validate the file extensions.
  $extensions = array_filter(array_map('trim', explode(',', $extensions)));
  if (empty($extensions)) {
    drush_set_error('extensions', dt('Specify the extensions of files to be included in the reviews.'));
    return FALSE;
  }
  // Make an associative array of file extensions.
  $extensions = array_combine($extensions, $extensions);

  return TRUE;
}
