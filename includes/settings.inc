<?php
/**
 * @file
 * Provides module conversion settings form.
 *
 * Copyright 2009-11 by Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

module_load_include('inc', 'secure_code_review', 'includes/utility');

/**
 * Implements hook_form_FORM_ID_alter().
 */
function _secure_code_review_form_coder_upgrade_settings_form_alter(&$form, &$form_state, $form_id) {
  $secure_code_review_fs = array(
    '#type' => 'fieldset',
    '#title' => t('Secure Code Review'),
    '#description' => t('These settings apply to review routines provided by the Secure Code Review module.'),
    '#tree' => FALSE,
    '#weight' => 2,
  );
  $path = secure_code_review_directory_path('', FALSE);
  $secure_code_review_fs['secure_code_review_dir'] = array(
    '#title' => t('Module base directory'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('secure_code_review_dir', SECURE_CODE_REVIEW_DIR),
    '#description' => t('Directory beneath the file system path (@path) in which are housed the old, new and patch directories. Default is @default. In the old directory, place the module code to be upgraded. The upgraded code is saved to the new directory and patch files are written to the patch directory. <strong>Note: this directory is only used when these reviews are invoked with Drush. Otherwise, the directory specified in the General settings tab is used.</strong>', array('@path' => $path, '@default' => SECURE_CODE_REVIEW_DIR)),
    '#size' => 30,
    '#maxlength' => 255,
    '#validate' => array('coder_upgrade_validate_dir'),
  );
  $secure_code_review_fs['secure_code_review_log_level'] = array(
    '#title' => t('Log level'),
    '#type' => 'radios', // '#type' => 'checkboxes',
    '#required' => TRUE,
    '#default_value' => variable_get('secure_code_review_log_level', SECURE_CODE_REVIEW_FAIL),
    '#description' => t('Review items will be logged if their review status equals or exceeds the indicated status. Statuses are shown in ascending order.'),
    '#options' => array(
      SECURE_CODE_REVIEW_PASS => secure_code_review_status_to_text(SECURE_CODE_REVIEW_PASS, 'long'),
      SECURE_CODE_REVIEW_UNCLEAR => secure_code_review_status_to_text(SECURE_CODE_REVIEW_UNCLEAR, 'long'),
      SECURE_CODE_REVIEW_FAIL => secure_code_review_status_to_text(SECURE_CODE_REVIEW_FAIL, 'long'),
    ),
  );
  $form['tabs']['secure'] = $secure_code_review_fs;

  // Make sure the submit handler runs before the system settings form handler.
  array_unshift($form['#submit'], 'secure_code_review_form_coder_upgrade_settings_form_submit');
}

/**
 * Submit handler for the settings form.
 *
 * Rename module input and output directories based on user settings.
 */
function secure_code_review_form_coder_upgrade_settings_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $op = isset($values['op']) ? $values['op'] : '';

  $cur = variable_get('secure_code_review_dir', SECURE_CODE_REVIEW_DIR);
  $new = $op == t('Reset to defaults') ? SECURE_CODE_REVIEW_DIR : $values['secure_code_review_dir'];
  if ($new != $cur) {
    if (rename(secure_code_review_directory_path($cur, FALSE), secure_code_review_directory_path($new, FALSE))) {
      variable_set('secure_code_review_dir_old', $new . '/old');
      variable_set('secure_code_review_dir_new', $new . '/new');
      variable_set('secure_code_review_dir_patch', $new . '/patch');
      drupal_set_message(t('Base directory was renamed to ' . $new . '.'));
    }
    else {
      // Reset the directory variable.
      variable_set('secure_code_review_dir', $cur);
      drupal_set_message(t('Could not rename base directory'), 'error');
    }
  }
}
