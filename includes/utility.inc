<?php
/**
 * @file
 * Provides constants and utility functions.
 *
 * Copyright 2009-11 by Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * Returns full directory path relative to sites directory.
 *
 * @param string $name
 *   Name of the directory.
 * @param boolean $add_slash
 *   Indicates whether to add a trailing slash.
 * @param boolean $stream_format
 *   Indicates whether to use the actual path or a stream protocol.
 *
 * @return string
 *   A string of the directory path.
 */
function secure_code_review_directory_path($name, $add_slash = TRUE, $stream_format = FALSE) {
  $slash = $add_slash ? '/' : '';
  $prefix_no_slash = $stream_format ? file_default_scheme() . ':/' : file_stream_path();
  $prefix = $prefix_no_slash . '/';

  switch ($name) {
    case 'base':
      return $prefix . variable_get('secure_code_review_dir', SECURE_CODE_REVIEW_DIR) . $slash;
    case 'old':
      return $prefix . variable_get('secure_code_review_dir_old', SECURE_CODE_REVIEW_OLD) . $slash;
    case 'new':
      return $prefix . variable_get('secure_code_review_dir_new', SECURE_CODE_REVIEW_NEW) . $slash;
    case 'patch':
      return $prefix . variable_get('secure_code_review_dir_patch', SECURE_CODE_REVIEW_PATCH) . $slash;
    case '':
      return $prefix_no_slash; // @todo Is this correct with a stream format?
    default:
      return $prefix . $name . $slash;
  }
}

/**
 * Prints log information if log flag is on.
 *
 * @todo
 * Include filename. (Will this be possible if we concatenate the files?)
 * Could we include filename in the tokens (except parser is filename agnostic).
 *
 * @param integer $line
 *   An integer of the line number being reviewed.
 * @param string $text
 *   A string to print.
 * @param integer $status
 *   An integer indicating the review status.
 */
function secure_code_review_log_print($line, $text, $status = SECURE_CODE_REVIEW_UNCLEAR) {
  global $_coder_upgrade_log, $_coder_upgrade_filename;
  static $path = '', $threshhold, $old_dir;

  if (!$_coder_upgrade_log) {
    return;
  }
  if (!$path) {
    $path = coder_upgrade_path('scr_log');
    coder_upgrade_path_clear('scr_log');
    $threshhold = variable_get('secure_code_review_log_level', SECURE_CODE_REVIEW_FAIL);
    $old_dir = DRUPAL_ROOT . '/' . coder_upgrade_directory_path('old');
  }
  if ($status < $threshhold) {
    return;
  }

  $backtrace = debug_backtrace();
  array_shift($backtrace);
  $caller = _drupal_get_last_caller($backtrace);
  if (($pos = strpos($caller['file'], 'secure_code_review')) !== FALSE) {
    $caller['file'] = substr($caller['file'], $pos);
  }

  $data = array(
    str_replace($old_dir, '', $_coder_upgrade_filename),
    $line,
    secure_code_review_status_to_text($status),
    $text,
    $caller['function'],
    $caller['line'],
    $caller['file'],
  );
  $item = json_encode($data);
  coder_upgrade_path_print($path, $item);

  // Also log to standard text file.
  coder_upgrade_log_print(sprintf('Line %4d: %4s: %s', $line, secure_code_review_status_to_text($status), $text));
}

function scrlp($line, $text, $status = SECURE_CODE_REVIEW_UNCLEAR, $description = '') {
  if ($description) {
    $description .= ' ==>'; // Use two '=' so easier to find in file.
    secure_code_review_log_print($line, $description, $status);
  }
  secure_code_review_log_print($line, $text, $status);
}

function xprintf($line, $text) {
  return sprintf('Line %4d: %s', $line, $text);
}

/**
 * Returns text corresponding to numeric status.
 *
 * @param integer $status
 *   An integer indicating the review status.
 * @param string $type
 *   A string indicating the type of text to return.
 *
 * @return string
 *   The corresponding text.
 */
function secure_code_review_status_to_text($status, $type = 'short') {
  switch ($status) {
    case SECURE_CODE_REVIEW_PASS:
      return $type == 'short' ? 'PASS' : 'Pass: The code passed the review for a particular vulnerability.';
      break;
    case SECURE_CODE_REVIEW_UNCLEAR:
      return $type == 'short' ? 'UNCL' : 'Unclear: The review was unable to determine whether the code contains a particular vulnerability.';
      break;
    case SECURE_CODE_REVIEW_FAIL:
      return $type == 'short' ? 'FAIL' : 'Fail: The code failed the review for a particular vulnerability.';
      break;
  }
}
