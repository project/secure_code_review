<?php
/**
 * @file
 * Provides tools to assist with review routines.
 *
 * These routines use the grammar parser and are version-agnostic with respect
 * to Drupal. An exception may be secure_code_review_traverse_op() which helps
 * with hook($op) style functions, many (or all?) of which were eliminated in
 * Drupal 7.x. However, this pattern may remain in contributed modules.
 *
 * Copyright 2009-11 by Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * Common routines (used with all reviews).
 */

/**
 * Prepares expression for secure text review.
 *
 * @param PGPNode $node
 *   The statement containing the expression to review.
 * @param integer $index
 *  Index of the function call parameter to review.
 * @param string $msg_context
 *   The context string to include in the log messages.
 */
function secure_code_review_secure_text_setup(&$node, $index = 0, $msg_context = '') { // NEEDS WORK
  clp("\n" . xprintf($node->line, 'Reviewing ' . $node->data->name['value']));
  cdp(__FUNCTION__);

  // Get the function call object.
  $item = &$node->data;

  if ($item->parameterCount() < $index + 1) {
    // Nothing to do.
    return;
  }

  $msg_context = $msg_context ? $msg_context : 'In parameter ' . ($index + 1) . ' to ' . $node->data->name['value'] . '()';

  $parameter = $item->getParameter($index)->stripComments();
  cdp($parameter->toString());
  secure_code_review_secure_text_review($node->data->parent, $parameter, $msg_context);
}

/**
 * Reviews an expression for safe use of check_plain() and t().
 *
 * @param PGPNode $statement
 *   The statement containing the expression to review.
 * @param PGPExpression $expression
 *   The expression to review.
 * @param string $msg_context
 *   The context string to include in the log messages.
 */
function secure_code_review_secure_text_review($statement, $expression, $msg_context = '') { // NEEDS WORK
  cdp(__FUNCTION__);
  // Create helper objects.
  $editor = PGPEditor::getInstance();

  /*
   * Count number of operands in the stripped parameter.
   * This avoids extranneous parentheses. (Are there other characters to ignore?)
   * Issue warning if:
   * - more than one operand
   * - operand is a variable not surrounded by check_plain()
   *   - or look for an assignment expression in the same block and see if it has check_plain()
   * - operand is T_CONSTANT_ENCAPSED_STRING and $string != check_plain($string)
   * - handle an assignment expression as the parameter (need to get everything after the assignment operator -- do we have a function for this?)
   *
   * With t(), replacements using @ (run through check_plain()) or % (HTML-escaped and highlighted)
   * - those with ! are literals
   * - we could set the replacement values to '' on the first two
   * - then compare $string = t() to check_plain($string)
   * - if they match we are good
   */

  if ($expression->countType('operand') > 1) {
    cdp('parameter has > one operand');
    $statement->insertStatementBefore($editor->commentToStatement('// ' . $msg_context . ': Safer to have a single operand expression wrapped in a call to check_plain().'));
    scrlp($statement->line, $msg_context . ': Safer to have a single operand expression wrapped in a call to check_plain().');
    return;
  }
  cdp('parameter has one operand');
  if ($expression->isType(T_CONSTANT_ENCAPSED_STRING)) {
    cdp('parameter is a string');
    // Parameter is a string.
    $string = trim($expression->toString(), "'\"");
    if ($string != filter_xss_admin($string)) { // if ($string != check_plain($string)) {
      cdp($string, '$string');
      cdp(filter_xss_admin($string), 'check_plain'); // cdp(check_plain($string), 'check_plain');
      $statement->insertStatementBefore($editor->commentToStatement('// ' . $msg_context . ': The string parameter is not check_plain() safe.'));
      scrlp($statement->line, $msg_context . ': The string parameter is not check_plain() safe.', SECURE_CODE_REVIEW_FAIL);
    }
    else {
      scrlp($statement->line, $msg_context . ': Everything looks safe.', SECURE_CODE_REVIEW_PASS);
    }
  }
  elseif ($expression->isType(T_VARIABLE)) {
    cdp('parameter is a variable');
    // @todo Look for an assignment expression in the same block and see if it has check_plain()
    $statement->insertStatementBefore($editor->commentToStatement('// ' . $msg_context . ': Has the variable parameter been passed through a sanitizing function such as t(), check_plain(), check_markup(), or filter_xss_admin()?'));
    scrlp($statement->line, $msg_context . ': Has the variable parameter been passed through a sanitizing function such as t(), check_plain(), check_markup(), or filter_xss_admin()?');
  }
  elseif ($expression->isType(T_FUNCTION_CALL)) {
    cdp('parameter is a function call');
    // @todo This won't be found if wrapped in parenthese???
    $call = $expression->findNode('operand');
    // Remove reference to actual code.
    // This also removes the parent reference.
    $call = $editor->expressionToStatement($call->toString())->getElement();
    cdp($call, '$call');
    if (!is_array($call->name) || $call->name['type'] == T_VARIABLE) {
      // The function being called is a variable function.
      $statement->insertStatementBefore($editor->commentToStatement('// ' . $msg_context . ': Has the parameter (part of the variable function call) been passed through a sanitizing function such as t(), check_plain(), check_markup(), or filter_xss_admin()?'));
      scrlp($statement->line, $msg_context . ': Has the parameter (part of the variable function call) been passed through a sanitizing function such as t(), check_plain(), check_markup(), or filter_xss_admin()?');
    }
    if (in_array($call->name['value'], array('check_plain', 'filter_xss_admin'))) {
      // Life is good.
      scrlp($statement->line, $msg_context . ' the parameter is wrapped by ' . $call->name['value'] . '.', SECURE_CODE_REVIEW_PASS);
    }
    elseif ($call->name['value'] == 't') {
      cdp('function call is t');
      // Life is less good.
      if (!$call->parameterCount()) {
        // Nothing to do.
        return;
      }
      $count = $call->parameterCount();
      if ($count == 1) {
        $string = trim($call->getParameter()->stripComments()->toString(), "'\"");
        if ($string != filter_xss_admin($string)) { // if ($string != check_plain($string)) {
          cdp($string, '$string');
          cdp(filter_xss_admin($string), 'check_plain');
          $statement->insertStatementBefore($editor->commentToStatement('// ' . $msg_context . ': The string parameter to t() is not check_plain() safe.'));
          scrlp($statement->line, $msg_context . ': The string parameter to t() is not check_plain() safe.', SECURE_CODE_REVIEW_FAIL);
        }
        else {
          scrlp($statement->line, $msg_context . ': Everything looks safe.', SECURE_CODE_REVIEW_PASS);
        }
        return;
      }

      $p0_string = trim($call->getParameter()->stripComments()->toString(), "'\"");
      $p1 = /*&*/ $call->getParameter(1);
      if (!$p1->isType(T_ARRAY)) {
        $statement->insertStatementBefore($editor->commentToStatement('// ' . $msg_context . ': WARNING: The second parameter to t() is not an inline array.'));
        scrlp($statement->line, $msg_context . ': WARNING: The second parameter to t() is not an inline array.', SECURE_CODE_REVIEW_FAIL);
        return;
      }
      $replacements = /*&*/ $p1->findNode('operand');
//      cdp($replacements->toString(), '$replacements');
      $unsafe = FALSE;
      cdp($replacements->count, '$replacements->count');
      for ($i = 0; $i < $replacements->count; $i++) {
        // @todo Could the key expression include a comment?
        $prefix = $replacements->getKey($i)->toString();
        cdp($prefix, '$prefix');
        $prefix = substr($replacements->getKey($i)->toString(), 1, 1); // First character is a quote.
        $value = &$replacements->getValue($i);
        if ($prefix == '@' || $prefix == '%') {
          // This replacement value is run through check_plain.
          $value->clear();
          $value->insertLast($editor->expressionToStatement("''"), 'operand');
          $p0_string = str_replace($prefix, 'xxx', $p0_string);
        }
        elseif ($prefix == '!') {
          // This replacement value is unsafe.
          $unsafe = TRUE;
          $value->clear();
          $value->insertLast($editor->expressionToStatement("''"), 'operand'); // @todo Should we inject some XSS unsafe text?
          $p0_string = str_replace($prefix, '<a>xxx</a>', $p0_string);
        }
      }
      if ($unsafe) {
        $statement->insertStatementBefore($editor->commentToStatement('// ' . $msg_context . ': One or more of the replacement parameters to t() is not guaranteed to be check_plain() safe.'));
        scrlp($statement->line, $msg_context . ': One or more of the replacement parameters to t() is not guaranteed to be check_plain() safe.', SECURE_CODE_REVIEW_FAIL);
      }
      // TODO check_plain() is not the function to use here as the text may legitimately include some HTML and characters that would be escaped (e.g. a quote).
      cdp($p0_string, '$p0_string');
      if ($p0_string != filter_xss_admin($p0_string)) { // if ($p0_string != check_plain($p0_string)) {
//        cdp($p0_string, '$p0_string');
        cdp(filter_xss_admin($p0_string), 'check_plain');
        $statement->insertStatementBefore($editor->commentToStatement('// ' . $msg_context . ': The string parameter to t() is not check_plain() safe.'));
        scrlp($statement->line, $msg_context . ': The string parameter to t() is not check_plain() safe.', SECURE_CODE_REVIEW_FAIL);
      }

//      cdp($replacements, '$replacements');
//      $string = $call->toString();
//      cdp($string, '$string');
//      //      $string = eval($string);
//      cdp($string, '$string after eval');
//      if ($string != check_plain($string)) {
//        cdp($string, '$string');
//        cdp(check_plain($string), 'check_plain');
//        $statement->insertStatementBefore($editor->commentToStatement('// ' . $msg_context . ': The string parameter to t() is not check_plain() safe.'));
//      }

    }
    else {
      // Not check_plain() or t().
      $statement->insertStatementBefore($editor->commentToStatement('// ' . $msg_context . ': Has the expression been passed through check_plain()?'));
      scrlp($statement->line, $msg_context . ': This function accepts unfiltered text; Has the expression been passed through a sanitizing function such as t(), check_plain(), check_markup(), or filter_xss_admin()?');
    }
  }
  else {
    // drupal_set_message() accepts unfiltered text; be sure to use t(), check_plain(), filter_xss_admin() or a similar function to ensure your $variable is fully sanitized.
    scrlp($statement->line, $msg_context . ': This function accepts unfiltered text; Has the expression been passed through a sanitizing function such as t(), check_plain(), check_markup(), or filter_xss_admin()?');
  }
}

/**
 * Reviews an expression for safe use of pattern modifiers in preg_replace().
 *
 * @param PGPNode $statement
 *   The statement containing the expression to review.
 * @param PGPExpression $expression
 *   The expression to review.
 * @param string $msg_context
 *   The context string to include in the log messages.
 */
function secure_code_review_secure_pattern_review($statement, $expression, $msg_context = '') { // NEEDS WORK
  cdp(__FUNCTION__);

  if (is_object($expression) && $expression->isType(T_CONSTANT_ENCAPSED_STRING)) {
    $string = $expression->toString();
  }
  elseif (is_array($expression) && $expression['type'] == T_CONSTANT_ENCAPSED_STRING) {
    $string = $expression['value'];
  }
  else {
    return;
  }

//   $string = $expression->toString();
  $delimiter = $string[1];
  $modifiers = substr($string, strrpos($string, $delimiter));
  cdp($string, '$string');
  cdp($modifiers, '$modifiers');
  if (strpos($modifiers, 'e') !== FALSE) {
    scrlp($statement->line, $msg_context . ': Use preg_replace_callback() instead of the insecure "e" modifier to preg_replace().', SECURE_CODE_REVIEW_FAIL);
  }
}

/**
 * Common routines (used with function reviews).
 */

/**
 * Initiates the review of a hook($op) style function.
 *
 * In D6, these functions are combo functions with separate blocks of code
 * delimited by the $op parameter in the condition of an if or switch statement.
 * Within each if or switch block is a separate function of sorts (as it is in
 * D7). Traverse the list of statements inside each $op block. Determine the
 * return variable by:
 * - searching in reverse order all statements in the block (without recursing
 *   into statements with bodies, e.g. if, for, foreach, etc.)
 * - if found, then traverse the block recursively looking for assignments
 *   to the return variable.
 * - if not found, then traverse the block recursively looking for return
 *   statements.
 * - if found, then back track looking for assignments to the return variable.
 *
 * The 3 main use case of convert_return() apply here as well. The difference
 * is the array code is nested inside other body statements, e.g. if or switch.
 * The convert_return() could encounter foreach loops in hook_menu().
 *
 * @param PGPNode $node
 *   A node object containing a PGPClass (or function) item.
 * @param string $callback
 *   A string of the callback function for the hook.
 * @param integer $op_index
 *   An integer of the operation parameter in the function parameter list.
 */
function secure_code_review_traverse_op(&$node, $callback, $op_index) {
  cdp("inside " . __FUNCTION__);
  cdp("$callback");

  // Get the function object.
  $item = &$node->data;

  // Get the operation variable from the function parameter at index $op_index.
  // This function removes any default value assignment (e.g. $op = 'list') or
  // inline comments included in the parameter expression.
  if (!($variable = $item->getParameterVariable($op_index))) {
    clp("ERROR: Variable not found in hook(\$op) parameter $op_index");
    return;
  }
  $op = $variable->toString();

  // Get the function body statements.
  $body = &$item->body;

  /*
   * Two likely cases: switch statement or series of if blocks.
   * Compare the $op_index parameter to the function with the switch operand.
   */

  // Loop on the body statements looking for the $op variable in an IF or
  // SWITCH condition.
  $current = $body->first();
  while ($current->next != NULL) {
    $found = FALSE;
    $statement = &$current->data;
    if ($statement instanceof PGPConditional) {
//      cdp("inside PGPConditional check");
//      cdp("statement->type = " . $statement->type);
      if ($statement->type == T_SWITCH) {
//        cdp("inside T_SWITCH check");
        // Get the list of conditions.
        $conditions = $statement->conditions;
        // Get the first condition. (With a switch there should only be one condition.)
        $condition = $conditions->getElement()->findNode('operand')->stripComments();
        $operand = $condition->toString();
        // If the condition variable matches the $op variable, then go to work.
        if ($op == $operand) {
          $found = TRUE;
          $cases = $statement->body;
          $node->traverse($cases, $callback);
        }
      }
      elseif (in_array($statement->type, array(T_IF, T_ELSEIF, T_ELSE_IF/*, T_ELSE*/))) {
        cdp("inside T_IF check");
        /*
         * Extract the conditions referencing the $op variable and loop on them.
         * These are conditions of the form $op == 'operation'.
         * Replace them with condition of TRUE to not disrupt the logic.
         * Retain any other conditions as part of the body in the new hook
         * function.
         */
        $operations = secure_code_review_extract_operations($statement->conditions, $op);
        // Loop on the extracted operations.
        foreach ($operations as $operation) {
          // Change a T_ELSEIF to a T_IF in the new hook function.
//          $statement->type = T_IF; // If it isn't already.
//          $block = new stdClass();
//          $block->body = new PGPBody();
//          $block->body->insertLast($statement);
//          $case_node = new PGPNode($block, $current->container); // TODO What is the correct container???
          $callback($node, $current, $operation); // $callback($node, $case_node, $operation);
        }
      }
    }
    // Move to next node.
    $current = &$current->next;
  }
}

/**
 * Extracts operations from conditions [and replaces the conditions with TRUE].
 *
 * @param PGPList $conditions
 *   A list of conditions to an if block.
 * @param string $op
 *   A string of the hook operation.
 * @return array
 *   Array of operations referenced in the if block.
 */
function secure_code_review_extract_operations(&$conditions, $op) {
  cdp("inside " . __FUNCTION__);
  $operations = array();

  /*
   * A condition may consist of at most two operands separated by an operator.
   */
  if ($conditions instanceof PGPList) {
    // Iterate over the conditions of the condition list.
    $current = $conditions->first();
    while ($current->next != NULL) {
      $type = $current->type;
      if ($type == 'condition') {
        // Get the condition object of the current node.
        $condition = &$current->data;
        // Iterate over elements of the condition expression.
        $found = FALSE;
        $current2 = $condition->first();
        while ($current2->next != NULL) {
          if ($current2->type == 'operand') {
            // Get the operand (object or array) of the current node.
            $element = &$current2->data;
            // Inspect the element looking for $op.
            if ($element instanceof PGPOperand) {
              // Inspect the operand looking for $op.
              $text = $element->toString();
              if (strpos($text, $op) !== FALSE) {
                $found = TRUE;
              }
              else {
                $operation = $element->toString();
              }
            }
            elseif (is_array($element)) {
              // This should have type = T_CONSTANT_ENCAPSED_STRING.
              $operation = $element['value'];
            }
          }
          // An interesting effect takes place with an & on the next line.
          $current2 = /*&*/ $current2->next;
        }
        if ($found) {
          // Replace condition with TRUE so the logic remains the same.
//          $condition->clear();
//          $data = array(
//            'type' => T_STRING,
//            'value' => 'TRUE',
//          );
//          $condition->insertLast($data, 'operand');

          // Add operation to list.
          $operations[] = trim($operation, "'\"");
        }
      }
      $current = /*&*/ $current->next;
    }
  }

  return $operations;
}

/**
 * Initiates the transformation of array assignments in a hook.
 *
 * Applies to: hook_action_info(), hook_hook_info(), hook_node_info(), hook_theme().
 *
 * NOTE
 * In general, there are 3 typical cases (or code styles):
 * - return array('key1' => array('key2' => ...);
 * - $var = array('key1' => array('key2' => ...); return $var;
 * - $var = array(); $var['key1'] = array('key2' => ...); return $var;
 *
 * The inner array to modify is 3 levels deep in the first 2 cases, but only
 * 2 levels deep in the third. In the first 2 cases, we can loop on the key1
 * arrays. In the third, the loop is on assignment statements.
 *
 * This new routine was failing on hook_hook_info() because the keys are not
 * distinguishable. Ie, we can not tell what level of the array we are on if
 * the array is inline and has 3 levels. Previously, return_case1() would strip
 * off the first layer and get to the second level. There is no guarantee of
 * nice code anyway.
 *
 * This new routine was failing on capturing drupal_get_form() callbacks
 * on case 5, like example_admin_form3() defined in
 * $items[$admin_path]['page arguments'][] = 'example_admin_form3'. This again
 * relates to a depth parameter and that we are always looking for an array.
 *
 * Should we pass a depth parameter, or figure it dynamically?
 *
 * @param PGPList $body
 *   List of statements in a block.
 * @param string $hook
 *   Name of the hook being modified.
 * @param string $callback
 *   A string of the callback function for the hook.
 * @param integer $start_depth
 *   The starting depth of nested arrays to traverse.
 * @param integer $remaining_depth
 *   The remaining depth of nested arrays to traverse. When equal to zero, stop.
 */
function secure_code_review_traverse_return(&$body, $hook, $callback = '', $start_depth = 0, $remaining_depth = -1) { // DONE
  cdp("inside " . __FUNCTION__);

  // Initialize.
  $editor = PGPEditor::getInstance();
  $callback = $callback == '' ? "secure_code_review_callback_$hook" : $callback;
  $msg = '// TODO The array elements in this hook function need to be changed.';

  // Get a list of return statements in the body statements.
  $nodes = $body->searchAll('PGPFunctionCall', 'name', 'value', 'return', TRUE);

  // Keep track of return variables to avoid redundant searching.
  $already_searched = array();

  while (!empty($nodes)) {
    cdp('while (!empty($nodes)) ' . __FUNCTION__);
    $return_node = array_shift($nodes);
    $return = $return_node->data;

    // Evaluate the return operand.
    if (get_class($return) == 'PGPFunctionCall') {
      $value = /*&*/$return->getParameter();
      $depth = 0;
    }
    elseif (get_class($return) == 'PGPAssignment') {
      // Get the operands to the right of the assignment operator.
      $value = $return->getValue();
      cdp($return->toString(), '$return AFTER');

      // Evaluate the "depth" of the assignment based on number of indices in
      // the assignment variable.
      // Examples: the operand on the RHS is at
      //   level 1: $var = array(key => array(..), ..)
      //   level 2: $var[key1] = array(..)
      //   level 3: $var[key1][key2] = array(..)
      // This mimics what was done in return_caseN(). We went down to level 2
      // from case1 to case3.
      $assign_variable = $return->values->getElement()->getType('operand')->stripComments();
      // @todo This should handle most cases, but will fail depending on code style.
      // If depth is > start_depth (like case5), then reconstruct an array at
      // the desired depth using toString() and reparsing (see case5).
      $depth = $assign_variable->countType('index');
      if ($depth > $start_depth) {
        // @todo This works in some use cases (menu stuff in begin.inc). If we
        // need to change the original expression, this fails because $value is
        // now disjoint from the original expression.
        $value = secure_code_review_reconstruct_array($assign_variable, $value, $start_depth);
        $depth = $start_depth;
      }
    }
    cdp($value->toString(), '$value');

    $occurrence = 1;
    // Loop on all operands in expression (e.g. $array + array(..)).
    while ($occurrence < $value->countType('operand') + 1) {
      $operand = $value->getType('operand', $occurrence);
      if ($operand) {
        cdp('inside if ($operand)');
        if (!is_object($operand)) {
          // @todo This hits stuff like $items[$admin_path]['page arguments'][] = 'example_admin_form3';
          cdp('!is_object($operand)');
          cdp($operand, '$operand');
          $occurrence++;
          continue;
        }
        cdp($operand->toString(), '$operand');
        if (get_class($operand) == 'PGPArray') {
          // Use case 1 - returns array directly.
          clp("\n" . xprintf($return_node->line, 'Reviewing array expression'));
          $operand->traverse2($return_node, $hook, $callback, $start_depth - $depth, $remaining_depth);
        }
        elseif (get_class($operand) == 'PGPOperand') {
          // Avoid redundant searching.
          if (in_array($operand->stripComments()->toString(), $already_searched)) {
            $occurrence++;
            continue;
          }

          /*
           * Search body statements for all assignments to the return variable.
           * The assignment could be to an array element like $info['node_type_name'] = array(...).
           * Or directly to the variable like $info = array('node_type_name' => array(...)).
           */

          $already_searched[] = $return_variable = $operand->stripComments()->toString();
          // @todo Limit search to nodes preceding the statement.
          $nodes = array_merge($nodes, $body->searchAll('PGPAssignment', 'values', 0, $return_variable, TRUE/*, 'backward', $parent*/));
        }
        else {
          clp('Line ' . $return_node->line,  " ERROR: operand of return statement is not an array or variable in hook_$hook");
          cdp('Line ' . $return_node->line . " ERROR: operand of return statement is not an array or variable in hook_$hook");
          cdp($operand, '$operand');
          $body->insertFirst($editor->commentToStatement($msg), 'comment');
        }
      }
      $occurrence++;
    }
  }
}

/**
 * Traverses a statement list in search of assignments to specified variables.
 *
 * The statement list is typically a function body. The specified variables may
 * include array keys. If an assignment is found, then the value is passed to a
 * callback routine.
 *
 * With array keys, to simplify the analyis, reconstruct an array to the desired
 * depth.
 * @code
 *   $foo = array('key-of-interest' => $value-of-interest)
 * @endcode
 *
 * Applies to: theme().
 *
 * NOTE
 * In general, there are 3 typical cases (or code styles):
 * - return array('key1' => array('key2' => ...);
 * - $var = array('key1' => array('key2' => ...); return $var;
 * - $var = array(); $var['key1'] = array('key2' => ...); return $var;
 *
 * The inner array to modify is 3 levels deep in the first 2 cases, but only
 * 2 levels deep in the third. In the first 2 cases, we can loop on the key1
 * arrays. In the third, the loop is on assignment statements.
 *
 * This new routine was failing on hook_hook_info() because the keys are not
 * distinguishable. Ie, we can not tell what level of the array we are on if
 * the array is inline and has 3 levels. Previously, return_case1() would strip
 * off the first layer and get to the second level. There is no guarantee of
 * nice code anyway.
 *
 * This new routine was failing on capturing drupal_get_form() callbacks
 * on case 5, like example_admin_form3() defined in
 * $items[$admin_path]['page arguments'][] = 'example_admin_form3'. This again
 * relates to a depth parameter and that we are always looking for an array.
 *
 * Should we pass a depth parameter, or figure it dynamically?
 *
 * @param PGPList $list
 *   List of statements in a block.
 * //@param string $variable
 * //  Name of the variable to find.
 * @param PGPExpression $value
 *   Expression with the variables to find.
 * @param integer $line
 *   The line number of the node containing $value.
 * @param string $hook
 *   Name of the hook being modified.
 * @param string $callback
 *   A string of the callback function for the hook.
 * @param integer $start_depth
 *   The starting depth of nested arrays to traverse.
 * @param integer $remaining_depth
 *   The remaining depth of nested arrays to traverse. When equal to zero, stop.
 */
function secure_code_review_list_traverse(&$list, $value, $line, $hook, $callback = '', $start_depth = 0, $remaining_depth = -1) { // DONE
  cdp("inside " . __FUNCTION__);

  // Initialize.
  $editor = PGPEditor::getInstance();
  $callback = $callback == '' ? "secure_code_review_callback_$hook" : $callback;
  $msg = '// TODO The array elements in this hook function need to be changed.';

  // Seed the list of variable assignments to find in the statement list.
  $nodes = array();
  $temp = $editor->textToStatements('$foo = ' . $value->toString())->get(0);
  $temp->line = $line;
  $nodes[] = $temp;

  // Keep track of return variables to avoid redundant searching.
  $already_searched = array();

  while (!empty($nodes)) {
    cdp('while (!empty($nodes)) ' . __FUNCTION__);
    $return_node = array_shift($nodes);
    $return = $return_node->data;

    // Evaluate the return operand.
    if (get_class($return) == 'PGPFunctionCall') {
      $value = /*&*/$return->getParameter();
      $depth = 0;
    }
    elseif (get_class($return) == 'PGPAssignment') {
      // Get the operands to the right of the assignment operator.
      $value = $return->getValue();
      cdp($return->toString(), '$return AFTER');

      // Evaluate the "depth" of the assignment based on number of indices in
      // the assignment variable.
      // Examples: the operand on the RHS is at
      //   level 1: $var = array(key => array(..), ..)
      //   level 2: $var[key1] = array(..)
      //   level 3: $var[key1][key2] = array(..)
      // This mimics what was done in return_caseN(). We went down to level 2
      // from case1 to case3.
      $assign_variable = $return->values->getElement()->getType('operand')->stripComments();
      // @todo This should handle most cases, but will fail depending on code style.
      // If depth is > start_depth (like case5), then reconstruct an array at
      // the desired depth using toString() and reparsing (see case5).
      $depth = $assign_variable->countType('index');
      if ($depth > $start_depth) {
        // @todo This works in some use cases (menu stuff in begin.inc). If we
        // need to change the original expression, this fails because $value is
        // now disjoint from the original expression.
        $value = secure_code_review_reconstruct_array($assign_variable, $value, $start_depth);
        $depth = $start_depth;
      }
    }
    cdp($value->toString(), '$value');

    $occurrence = 1;
    // Loop on all operands in expression (e.g. $array + array(..)).
    while ($occurrence < $value->countType('operand') + 1) {
      $operand = $value->getType('operand', $occurrence);
      if ($operand) {
        cdp('inside if ($operand)');
        if (!is_object($operand)) {
          // @todo This hits stuff like $items[$admin_path]['page arguments'][] = 'example_admin_form3';
          cdp('!is_object($operand)');
          cdp($operand, '$operand');
          $occurrence++;

          $expression = new PGPExpression();
          $expression->insertLast($operand, 'operand');
          cdp($expression, '$expression');
          secure_code_review_secure_text_review($return_node, $expression, 'In the "' . $operand['value'] . '" assignment');

          continue;
        }
        cdp($operand->toString(), '$operand');
        if (get_class($operand) == 'PGPArray') {
          // Use case 1 - returns array directly.
          clp("\n" . xprintf($return_node->line, 'Reviewing array expression'));
          $new_operands = $operand->traverse2($return_node, $hook, $callback, $start_depth - $depth, $remaining_depth);
          foreach ($new_operands as $new_operand) {
            // @todo Refactor this code to a routine. (Also found in next elseif block.)
            // Avoid redundant searching.
            if (in_array($new_operand->stripComments()->toString(), $already_searched)) {
//               $occurrence++;
              continue;
            }
            $already_searched[] = $return_variable = $new_operand->stripComments()->toString();
            // @todo Limit search to nodes preceding the statement.
            $nodes = array_merge($nodes, $list->searchAll('PGPAssignment', 'values', 0, $return_variable, TRUE/*, 'backward', $parent*/));
          }
        }
        elseif (get_class($operand) == 'PGPOperand') {
          // Avoid redundant searching.
          if (in_array($operand->stripComments()->toString(), $already_searched)) {
            $occurrence++;
            continue;
          }

          // The operand is not an array:
          // - inspect its value for XSS vulnerability (it may be the final assignment in this list)
          // - traverse the list looking for assignments to this variable
          // - check if this is a parameter to the function (if applicable) containing the list
          $expression = new PGPExpression();
          $expression->insertLast($operand, 'operand');
          cdp($expression, '$expression');
          secure_code_review_secure_text_review($return_node, $expression, 'In the "' . $operand->toString() . '" assignment');

          /*
           * Search body statements for all assignments to the return variable.
           * The assignment could be to an array element like $info['node_type_name'] = array(...).
           * Or directly to the variable like $info = array('node_type_name' => array(...)).
           */

          $already_searched[] = $return_variable = $operand->stripComments()->toString();
          // @todo Limit search to nodes preceding the statement.
          $nodes = array_merge($nodes, $list->searchAll('PGPAssignment', 'values', 0, $return_variable, TRUE/*, 'backward', $parent*/));
        }
        else {
          clp('Line ' . $return_node->line,  " ERROR: operand of return statement is not an array or variable in hook_$hook");
          cdp('Line ' . $return_node->line . " ERROR: operand of return statement is not an array or variable in hook_$hook");
          cdp($operand, '$operand');
          $list->insertFirst($editor->commentToStatement($msg), 'comment');
        }
      }
      $occurrence++;
    }
  }
}

/**
 * Returns array with another level whose key is from assignment variable.
 *
 * @param PGPExpression $variable
 *   The assignment variable (left of assignment operator).
 * @param PGPExpression $value
 *   The assignment value (right of assignment operator).
 * @param integer $target_depth
 *   The desired depth of the array to return.
 *
 * @return PGPExpression
 *   The new array expression.
 */
function secure_code_review_reconstruct_array($variable, $value, $target_depth) {
  cdp("inside " . __FUNCTION__);

  if ($variable->countType('index') < 1) {
    return new PGPExpression();
  }

  $editor = PGPEditor::getInstance();

  // If the key is not a simple quoted string, then we need to respect the expression.
  for ($index = $variable->countType('index'); $index > $target_depth; $index--) {
    $key = $variable->getType('index', $index)->stripComments();
    if ($key->isEmpty()) {
      $array = $editor->expressionToStatement("array({$value->toString()})"); //->getElement();
    }
    else {
      $array = $editor->expressionToStatement("array({$key->toString()} => {$value->toString()})"); //->getElement();
    }
    $value = $array;
  }

  return $array;
}
