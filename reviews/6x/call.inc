<?php
/**
 * @file
 * Provides review routines applied to function calls.
 *
 * These routines use the grammar parser.
 *
 * Copyright 2009-11 by Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * The reviews to these functions are documented at the following urls.
 * These reviews assume D6 version code.
 *
 * http://drupal.org/node/28984 (Handle text in a secure fashion)
 * http://api.drupal.org/api/function/drupal_set_header/6 (include character set when sending content type)
 *
 */

/**
 * Implements hook_upgrade_call_alter().
 */
function secure_code_review_upgrade_call_alter(&$node, &$reader, $name) { // NEEDS WORK
  switch ($name) {
    case 'confirm_form':
      // confirm_form($form, $question, $path, $description = NULL, $yes = NULL, $no = NULL, $name = 'confirm')
      foreach (array(1, 3, 4, 5) as $index) {
        secure_code_review_secure_text_setup($node, $index);
      }
      break;

    case 'drupal_set_message':
    case 'drupal_set_title':
      $index = 0;
      secure_code_review_secure_text_setup($node, $index);
      break;

    case 'form_error':
    case 'form_set_error':
      $index = 1;
      secure_code_review_secure_text_setup($node, $index);
      break;

    case 'db_query':
    case 'db_query_range':
    case 'pager_query':
      $index = 0;
      secure_code_review_node_query_review($node, $index);
      break;

    default:
      break;
  }
}

/**
 * Implements hook_upgrade_call_drupal_set_header_alter().
 */
function secure_code_review_upgrade_call_drupal_set_header_alter(&$node, &$reader) { // DONE
  clp("\n" . xprintf($node->line, 'Reviewing ' . $node->data->name['value']));
  // Create helper objects.
  $editor = PGPEditor::getInstance();

  // Get the function call object.
  $item = &$node->data;

  if (!$item->parameterCount()) {
    // Nothing else to do.
    return;
  }

  $temp = $item->printParameter();
  $p0 = $item->getParameter();
  if ($p0->count() > 1) {
    // This is a complex expression.
  }
  else {
    if ($p0->isType(T_CONSTANT_ENCAPSED_STRING)) {
      if (strpos($temp, 'HTTP/') !== FALSE) {
      }
      elseif (strpos($temp, ':') !== FALSE) {
        list($type, $value) = explode(':', $temp);
        $type = trim($type, "'\" ");
        $value = trim($value, "'\" ");
        if ($type == 'Content-Type' && strpos($value, 'charset') === FALSE) {
          $statement = $node->data->parent;
          $msg_context = 'In parameter 1 to ' . $node->data->name['value'] . '()';
          scrlp($statement->line, $msg_context . ': When sending a Content-Type header, always include a \'charset\' type, too. This is necessary to avoid security bugs (e.g. UTF-7 XSS).', SECURE_CODE_REVIEW_FAIL);
          $value .= '; charset=utf-8';
        }
        $type = "'$type'";
        $value = "'$value'";
        $editor->setParameters($item, array($type, $value));
      }
    }
    else {
      // Could be a variable that we could search for and change?
    }
  }
}

/**
 * Implements hook_upgrade_call_preg_replace_alter().
 */
function secure_code_review_upgrade_call_preg_replace_alter(&$node, &$reader) { // NEEDS WORK
  clp("\n" . xprintf($node->line, 'Reviewing ' . $node->data->name['value']));
  cdp(__FUNCTION__);

  // Get the function call object.
  $item = &$node->data;

  if (!$item->parameterCount()) {
    // Nothing to do.
    return;
  }

  $found = FALSE;
  $pattern = $item->getParameter()->stripComments();
  if ($pattern->isType(T_CONSTANT_ENCAPSED_STRING)) {
    // Parameter is a string.
    secure_code_review_secure_pattern_review($item->parent, $pattern, 'On this pattern used in preg_replace()');
    $found = TRUE;
  }
  elseif ($pattern->isType(T_VARIABLE)) {
    // Parameter is a variable.
    $variable = $pattern->findNode('operand')->toString();
    // Get the parent = statement (i.e. node) this function call is part of.
    $parent = $item->parent;
    // Search for an assignment statement to this variable.
    $statement = $parent->container->searchBackward('PGPAssignment', 'values', 0, $variable, $parent, TRUE);
    if ($statement) {
      $pattern = $statement->data->getValue();
      // TODO A pattern here - this is the same code as above but executed on a different object.
      if ($pattern->isType(T_CONSTANT_ENCAPSED_STRING)) {
        secure_code_review_secure_pattern_review($statement, $pattern, 'On this pattern used in the preg_replace() below');
        $found = TRUE;
      }
    }
  }

  if (!$found) {
    scrlp($node->line, 'WARNING: Could not find a pattern string to review in preg_replace(). Please confirm it does not use the insecure "e" modifier.');
  }
}

/**
 * Implements hook_upgrade_call_theme_alter().
 *
 * Vulnerable parameters (i,e, expecting sanitized values)
 * Example: theme_table($header, $rows, $attributes, $caption)
 * - 'data' elements of header (or the element if not an array)
 * - 'data' elements of rows (or the element if not an array)
 * - $caption
 *
 * We need to analyze the value associated with the above keys or the value if
 * the cell is not an array. This will likely require a more robust version of
 * secure_code_review_reconstruct_array() and a modified version of
 * secure_code_review_traverse_return() to collect all the variable assignments.
 * Can we refactor the tool,inc routines into a common set shared by this and
 * coder upgrade?
 *
 * Refactor secure_code_review_traverse_return() to separate the return variables
 * from the traversal to find assignments. Pass the variables to look for and
 * the traversal will find others used in assignments.
 *
 * In D7, this will be one level more complicated as the arguments to theme()
 * are all wrapped into $variables.
 *
 * Steps
 * - determine the variables being passed to theme()
 * - unwrap $variables into $header, $rows, etc.
 * - filter the variables that are not vulnerable (i.e. the theme function
 *   sanitizes them or otherwise does not output them as vulnerable html)
 * - determine the keys of interest for each parameter (or the parameter)
 * - foreach vulnerable parameter, recursivley traverse the function body
 *   looking for assignments to $variable[$key] (or $variable)
 * - with each assignment, recursivley traverse the function body in an attempt
 *   to find the ultimate assignment
 * - do secure text review on the latter
 * - 
 * - catalog the vulnerable keys or parameters for each core theme function
 * - if key is blank, then entire variable
 * - if key may be present, how to tell ourselves? use "is_key_optional" flag
 * - if optional, then need to traverse further to see if a key is used
 */
function secure_code_review_upgrade_call_theme_alter(&$node, &$reader) { // DONE
  clp("\n" . xprintf($node->line, 'Reviewing ' . $node->data->name['value']));
  // Create helper objects.
  $editor = PGPEditor::getInstance();

  // Get the function call object.
  $item = &$node->data;

  if (!$item->parameterCount()) {
    // Nothing to do.
    return;
  }

  // Get theme name.
  $parameter = $item->getParameter(); // ->stripComments();
  if (!$parameter->isType(T_CONSTANT_ENCAPSED_STRING)) {
    // @todo Search for string assignment to variable.
    return;
  }

  $theme = $parameter->trim();
  if ($theme != 'table') {
//     return;
  }

  // Grab the function object containing the theme function call.
  if (!($function = $reader->findFunction($node->line))) {
    return;
  }

  // Retrieve list of vulnerable parameters for the theme.
  if (!($vulnerables = secure_code_review_theme_info($theme))) {
    return;
  }

  foreach ($vulnerables as $key => $vulnerable) {
    if ($vulnerable['index'] > $item->parameterCount() - 1) {
      break;
    }
    $parameter = $item->getParameter($vulnerable['index'])->stripComments();
    cdp($parameter, '$parameter');

    if (!isset($vulnerable['keys'])) {
      $start_depth = $remaining_depth = 0;
      // @todo Refactor this block to a new function.
      // Use cases
      // - inline value: array('Title 1', array('data' => 'Title 2', ...), ...)
      // - variable: $header
      if ($parameter->isType(T_VARIABLE)) {
        $list = $function->body;
        // @todo $hook has no context here
        $hook = 'theme';
        $callback = isset($vulnerable['callback']) ? $vulnerable['callback'] : '';
        secure_code_review_list_traverse($list, $parameter, $node->line, $hook, $callback, $start_depth, $remaining_depth);
      }
      else {
        // @todo Evaluate inline expression.
      }
      continue;
    }
    foreach ($vulnerable['keys'] as $vulnerable_key) {
      $start_depth = $vulnerable_key['depth'];
      $remaining_depth = $vulnerable_key['remaining_depth'];
      // @todo Refactor this block to a new function.
      // Use cases
      // - inline value: array('Title 1', array('data' => 'Title 2', ...), ...)
      // - variable: $header
      if ($parameter->isType(T_VARIABLE)) {
        $list = $function->body;
        // @todo $hook has no context here
        $hook = 'theme';
        $callback = isset($vulnerable['callback']) ? $vulnerable['callback'] : '';
        secure_code_review_list_traverse($list, $parameter, $node->line, $hook, $callback, $start_depth, $remaining_depth);
      }
      else {
        // @todo Evaluate inline expression.
      }
    }
    
  }
}

/**
 * Returns list of vulnerable parameters and their keys (for a core theme).
 *
 * Zero base number the parameters relative to theme('name', ...).
 *
 * @param string $theme
 *   Theme name.
 *
 * @return array
 *   List of vulnerable parameters and their keys. Each item is an associative
 *   array that may contain the following key-value pairs:
 *   - "index": the zero-based index of the parameter in theme('foo', ...).
 *   - "keys": Each item is an associative array that may contain the following
 *     key-value pairs:
 *     - "key": (optional)
 *     - "optional": (optional) A Boolean indicating whether the key is optional.
 *     - "depth": (optional) The zero-based array depth at which the key is found.
 *   - "multiple": A Boolean indicating whether the parameter is an array.
 *   - "callback": (optional) A callback function to process the value.
 *   - "": (optional)
 *   - "": (optional)
 */
function secure_code_review_theme_info($theme) {
  switch ($theme) {
    case 'table':
      return array(
        'header' => array(
          'index' => 1,
          'keys' => array(array('key' => 'data', 'optional' => TRUE, 'depth' => 0, 'remaining_depth' => 1)),
          'multiple' => FALSE,
          'callback' => 'secure_code_review_callback_theme_table',
        ),
        'rows' => array(
          'index' => 2,
          'keys' => array(array('key' => 'data', 'optional' => TRUE, 'depth' => /*1*/ 0, 'remaining_depth' => 2)),
          'multiple' => TRUE,
          'callback' => 'secure_code_review_callback_theme_table',
        ),
        'caption' => array(
          'index' => 4,
          'keys' => NULL,
          'multiple' => FALSE,
          'callback' => '', // This can go straight to text review
        ),
      );
    case 'item_list':
      return array(
        'items' => array(
          'index' => 1,
          'keys' => array(array('key' => 'data', 'optional' => TRUE, 'depth' => /*1*/ 0, 'remaining_depth' => 3)), // @todo Child nesting could be deeper.
          'multiple' => TRUE,
          'callback' => 'secure_code_review_callback_theme_item_list',
        ),
        'title' => array(
          'index' => 2,
          'keys' => array(array('key' => 'data', 'optional' => TRUE, 'depth' => 0, 'remaining_depth' => 1)),
          'multiple' => FALSE,
          'callback' => 'secure_code_review_callback_theme_item_list',
        ),
        'type' => array(
          'index' => 3,
          'keys' => NULL,
          'multiple' => FALSE,
          'callback' => '', // This can go straight to text review
        ),
        // The keys passed to drupal_attributes() are not sanitized.
        // How likely is it that a malicious user could infect the keys?
      );
    case 'form_element':
      return array(
        'element' => array(
          'index' => 1,
          'keys' => array(
            array('key' => '#foo', 'optional' => TRUE, 'depth' => 0, 'remaining_depth' => 1),
//             array('key' => '#title', 'optional' => TRUE, 'depth' => 0, 'remaining_depth' => 1),
//             array('key' => '#description', 'optional' => TRUE, 'depth' => 0, 'remaining_depth' => 1),
//             array('key' => '#required', 'optional' => TRUE, 'depth' => 0, 'remaining_depth' => 1),
          ),
          'multiple' => FALSE,
          'callback' => 'secure_code_review_callback_theme_form_element',
        ),
        'value' => array(
          'index' => 2,
          'keys' => NULL,
          'multiple' => FALSE,
          'callback' => '', // This can go straight to text review
        ),
        // The keys passed to drupal_attributes() are not sanitized.
        // How likely is it that a malicious user could infect the keys?
      );

    default:
      return FALSE; // array();
  }
}

/**
 * Reviews parameter values to theme call.
 *
 * @param PGPNode $node
 *   The node of the statement containing the array object.
 * @param PGPArray $array
 *   The array object containing the array element ($current).
 * @param PGPNode $current
 *   The node object of the array element.
 * @param string $hook
 *   The hook name.
 * @param string $type
 *   The type (key or value) of the array element.
 * @param string $key
 *   The key of the array element (or the most recent key).
 * @param string $value
 *   The value of the array element (or NULL if element is a key).
 */
function secure_code_review_callback_theme_table($node, &$array, &$current, $hook, $type, $key, $value) {
  cdp("inside " . __FUNCTION__);

  if (!($current instanceof PGPNode)) {
    clp("ERROR: current is not a PGPNode object in hook_$hook");
    return;
  }

//   cdp($node->data->toString(), '$node');
//   cdp($current->data->toString(), '$current->data');
//   cdp($type, '$type');
//   cdp($key, '$key');
//   cdp($value, '$value');

  if ($current->data->isType(T_ARRAY)) {
    // If $current is an array with a 'data' key, then ignore it now and catch it
    // on the depth traversal.
//     cdp("INFO: current is a PGPArray object in hook_$hook");
    return;
  }

  $found = FALSE;
  if ($type == 'value' && in_array($key, array('data', ''))) {
    cdp("Found a vulnerable key");
    cdp(xprintf($node->line, 'Reviewing type = ' . $type . ' with key = "' . $key . '" and value = ' . $value));
    clp(xprintf($node->line, 'Reviewing type = ' . $type . ' with key = "' . $key . '" and value = ' . $value));
    $msg_context = strtr('In the "@key" array item', array('@key' => $key ? $key : $value));
    secure_code_review_secure_text_review($node, $current->data, $msg_context);
    if ($current->data->isType(T_VARIABLE)) {
      // @todo If $current is a variable, then notify our caller
      // (PGPArray::traverse2) to return this variable to its caller
      // (secure_code_review_list_traverse()) so the latter will add it to its
      // list of "nodes" to search for.
      return TRUE;
    }
  }
}

/**
 * Reviews parameter values to theme call.
 *
 * @param PGPNode $node
 *   The node of the statement containing the array object.
 * @param PGPArray $array
 *   The array object containing the array element ($current).
 * @param PGPNode $current
 *   The node object of the array element.
 * @param string $hook
 *   The hook name.
 * @param string $type
 *   The type (key or value) of the array element.
 * @param string $key
 *   The key of the array element (or the most recent key).
 * @param string $value
 *   The value of the array element (or NULL if element is a key).
 */
function secure_code_review_callback_theme_item_list($node, &$array, &$current, $hook, $type, $key, $value) {
  cdp("inside " . __FUNCTION__);

  if (!($current instanceof PGPNode)) {
    clp("ERROR: current is not a PGPNode object in hook_$hook");
    return;
  }

//   cdp($node->data->toString(), '$node');
//   cdp($current->data->toString(), '$current->data');
//   cdp($type, '$type');
//   cdp($key, '$key');
//   cdp($value, '$value');

  if ($current->data->isType(T_ARRAY)) {
    // If $current is an array with a 'data' key, then ignore it now and catch it
    // on the depth traversal.
//     cdp("INFO: current is a PGPArray object in hook_$hook");
    return;
  }

  $found = FALSE;
  if ($type == 'value' && in_array($key, array('data', ''))) {
    cdp("Found a vulnerable key");
    cdp(xprintf($node->line, 'Reviewing type = ' . $type . ' with key = "' . $key . '" and value = ' . $value));
    clp(xprintf($node->line, 'Reviewing type = ' . $type . ' with key = "' . $key . '" and value = ' . $value));
    $msg_context = strtr('In the "@key" array item', array('@key' => $key ? $key : $value));
    secure_code_review_secure_text_review($node, $current->data, $msg_context);
    if ($current->data->isType(T_VARIABLE)) {
      // @todo If $current is a variable, then notify our caller
      // (PGPArray::traverse2) to return this variable to its caller
      // (secure_code_review_list_traverse()) so the latter will add it to its
      // list of "nodes" to search for.
      return TRUE;
    }
  }
}

/**
 * Reviews parameter values to theme call.
 *
 * @param PGPNode $node
 *   The node of the statement containing the array object.
 * @param PGPArray $array
 *   The array object containing the array element ($current).
 * @param PGPNode $current
 *   The node object of the array element.
 * @param string $hook
 *   The hook name.
 * @param string $type
 *   The type (key or value) of the array element.
 * @param string $key
 *   The key of the array element (or the most recent key).
 * @param string $value
 *   The value of the array element (or NULL if element is a key).
 */
function secure_code_review_callback_theme_form_element($node, &$array, &$current, $hook, $type, $key, $value) {
  cdp("inside " . __FUNCTION__);

  if (!($current instanceof PGPNode)) {
    clp("ERROR: current is not a PGPNode object in hook_$hook");
    return;
  }

//   cdp($node->data->toString(), '$node');
//   cdp($current->data->toString(), '$current->data');
//   cdp($type, '$type');
//   cdp($key, '$key');
//   cdp($value, '$value');

  if ($current->data->isType(T_ARRAY)) {
    // If $current is an array with a 'data' key, then ignore it now and catch it
    // on the depth traversal.
//     cdp("INFO: current is a PGPArray object in hook_$hook");
    return;
  }

  $found = FALSE;
  if ($type == 'value' && in_array($key, array('#title', '#description', '#required'))) {
    cdp("Found a vulnerable key");
    cdp(xprintf($node->line, 'Reviewing type = ' . $type . ' with key = "' . $key . '" and value = ' . $value));
    clp(xprintf($node->line, 'Reviewing type = ' . $type . ' with key = "' . $key . '" and value = ' . $value));
    $msg_context = strtr('In the "@key" array item', array('@key' => $key ? $key : $value));
    secure_code_review_secure_text_review($node, $current->data, $msg_context);
    if ($current->data->isType(T_VARIABLE)) {
      // @todo If $current is a variable, then notify our caller
      // (PGPArray::traverse2) to return this variable to its caller
      // (secure_code_review_list_traverse()) so the latter will add it to its
      // list of "nodes" to search for.
      return TRUE;
    }
  }
}

/**
 * Reviews sql expression for possible node access bypass.
 *
 * @param PGPNode $node
 *   The statement containing the expression to review.
 * @param integer $index
 *  Index of the function call parameter to review.
 * @param string $msg_context
 *   The context string to include in the log messages.
 */
function secure_code_review_node_query_review(&$node, $index = 0, $msg_context = '') { // NEEDS WORK
  clp("\n" . xprintf($node->line, 'Reviewing ' . $node->data->name['value']));
  cdp(__FUNCTION__);

  // Get the function call object.
  $item = &$node->data;

  if ($item->parameterCount() < $index + 1) {
    // Nothing to do.
    return;
  }

  $msg_context = $msg_context ? $msg_context : 'In parameter ' . ($index + 1) . ' to ' . $node->data->name['value'] . '()';

  $parameter = $item->getParameter($index)->stripComments();
  cdp($parameter->toString());
  if ($parameter->count() == 1) {
    // The parameter is a single item expression.
    // Ignore this check and proceed with evaluating the first operand.
  }

  if ($parameter->isType(T_FUNCTION_CALL)) {
    cdp('parameter is a function call');
    $operand = $parameter->findNode('operand');
    if ($operand->name['type'] == T_STRING) {
      if ($operand->name['value'] == 'db_rewrite_sql') {
        // Nothing to do.
        scrlp($node->line, $msg_context . ': This query is wrapped by db_rewrite_sql().', SECURE_CODE_REVIEW_PASS);
      }
      else {
        // What to do?
      }
    }
  }
  elseif ($parameter->isType(T_CONSTANT_ENCAPSED_STRING)) {
    cdp('parameter is a string');

    $operand = $parameter->findNode('operand');
    $sql = $operand['value'];
    $delimiter = $sql[0];
    // Trim won't work here, since it'll trim off the last ' if the string ends in '%s'
    $sql = substr($sql, 1, -1);
    cdp($sql, '$sql');

    if (strpos($sql, 'SELECT') !== 0) {
      clp(xprintf($node->line, 'NOT a SELECT query'));
      return;
    }
    preg_match('@SELECT\s+COUNT\s*\(\s*\*\s*\)\s+FROM@', $sql, $matches);
    cdp($matches, '$matches');
    if (preg_match('@SELECT\s+COUNT\s*\(\s*\*\s*\)\s+FROM@', $sql, $matches)) {
      clp(xprintf($node->line, 'is a SELECT COUNT(*) query'));
      return;
    }

    if (strpos($sql, '{node}') !== FALSE) {
      scrlp($node->line, $msg_context . ': A SELECT query involving the node table should typically be wrapped by db_rewrite_sql() to avoid access bypass security violations. Please confirm this query does not require this wrapper.');
    }
  }
  else {
    // What to do? Search for variable(s) making up the SQL statement.
  }
}
