<?php
/**
 * @file
 * Provides review routines applied to the directory before routines are applied to the files.
 *
 * These routines use the grammar parser.
 *
 * Copyright 2009-11 by Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * Implements hook_upgrade_begin_alter().
 *
 * @param array $item
 *   Array of a directory containing the files to convert.
 */
function secure_code_review_upgrade_begin_alter($item) {
  module_load_include('inc', 'coder_upgrade', 'conversions/begin');
  module_load_include('inc', 'coder_upgrade', 'conversions/tool');
  coder_upgrade_upgrade_begin_alter($item);
}
