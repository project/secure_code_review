<?php
/**
 * @file
 * Provides review routines applied to functions (or hooks).
 *
 * These routines use the grammar parser.
 *
 * Copyright 2009-11 by Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * The reviews to these functions are documented at the following urls.
 * These reviews assume D6 version code.
 *
 * http://drupal.org/node/28984 (Handle text in a secure fashion)
 * http://drupal.org/node/109157 (Access control in menu API)
 *
 */

/**
 * Implements hook_upgrade_hook_alter().
 */
function secure_code_review_upgrade_hook_alter(&$node, &$reader, $hook) {
  cdp("inside " . __FUNCTION__);

  global $_coder_upgrade_menu_registry, $_coder_upgrade_theme_registry;
  cdp($node->data->name);
  if (in_array($node->data->name, $_coder_upgrade_menu_registry)) {
    clp("\n" . xprintf($node->line, 'Reviewing form callback = ' . $node->data->name));
    secure_code_review_traverse_return($node->data->body, 'form', '', 1, 1);
  }
  elseif (strpos($node->data->name, 'theme_') === 0) {
    $theme_name = substr($node->data->name, strpos($node->data->name, "theme_") + 6);
    if (isset($_coder_upgrade_theme_registry[$theme_name])) {
//       secure_code_review_convert_theme_callback($node);
    }
  }
}

/**
 * Reviews form callback array values.
 *
 * @param PGPNode $node
 *   The node of the statement containing the array object.
 * @param PGPArray $array
 *   The array object containing the array element ($current).
 * @param PGPNode $current
 *   The node object of the array element.
 * @param string $hook
 *   The hook name.
 * @param string $type
 *   The type (key or value) of the array element.
 * @param string $key
 *   The key of the array element (or the most recent key).
 * @param string $value
 *   The value of the array element (or NULL if element is a key).
 */
function secure_code_review_callback_form($node, &$array, &$current, $hook, $type, $key, $value) { // NEEDS WORK
//   clp(xprintf($node->line, 'Reviewing ' . $type . ' = ' . ($type == 'key' ? $key : $value) . ' (line ' . $node->line . ')'));
  cdp("inside " . __FUNCTION__);

  if (!($current instanceof PGPNode)) {
    clp("ERROR: current is not a PGPNode object in hook_$hook");
    return;
  }

  cdp($node->data->toString(), '$node');
  cdp($current->data->toString(), '$current->data');
  cdp($type, '$type');
  cdp($key, '$key');
  cdp($value, '$value');
//  return;

  $found = FALSE;
  // Not all values for '#default_value' and '#value' keys are vulnerable.
  // Save the '#type' value (hopefully it precedes everything else) so we know
  // whether to test the '#default_value' and '#value' values.
  if ($type == 'value' && in_array($key, array('#title', '#description', '#default_value', '#value', '#prefix', '#suffix', '#options'))) {
    clp(xprintf($node->line, 'Reviewing type = ' . $type . ' with key = ' . $key . ' and value = ' . $value));
    cdp("Found a vulnerable key");
    cdp(xprintf($node->line, 'Reviewing type = ' . $type . ' with key = ' . $key . ' and value = ' . $value));
    secure_code_review_secure_text_review($node, $current->data, 'In the "' . $key . '" array item');
  }
}

/**
 * Implements hook_upgrade_hook_block_alter().
 *
 * Values for $op
 * 'list': A list of all blocks defined by the module.
 * 'configure': Configuration form for the block.
 * - need to check form items
 * 'save': Save the configuration options.
 * 'view': Process the block when enabled in a region in order to view its contents.
 * - the 'subject' element is not safe
 */
function secure_code_review_upgrade_hook_block_alter(&$node, &$reader) {
  clp("\n" . xprintf($node->line, 'Reviewing ' . $node->data->name));
  // Changes: block_deltas_renamed
  // Changes: remove_op
  $callback = 'secure_code_review_callback_block';
  $op_index = 0;
  secure_code_review_traverse_op($node, $callback, $op_index);
}

/**
 * Reviews hook_block().
 *
 * Once we have the block of code, the analysis proceeds
 * - determine if the block body has a return statement
 *   - save the return variable (like convert_return)
 * - if not, then individual returns appear inside if or case blocks
 * - traverse the body statements
 *   - if return variable is not set, then find it inside the if or case block
 *   - find an array item with key = 'subject'
 *   - inspect the value for check_plain() or t() as with drupal_set_title()
 *   - find an array item with key = 'content'
 *   - if a function call, then evaluate the form code
 *   - need to generalize the inspection routines to locate and return
 *     - then the caller can make changes as appropriate
 *     - this would help coder_upgrade too
 */
function secure_code_review_callback_block($node, $case_node, $operation = '') {
  cdp("inside " . __FUNCTION__);

  // Loading this file will also enable the upgrade routines.
  // To avoid this, the "API routines" need to be moved to a separate file
  // (which could be one of the PGP classes).
//  module_load_include('inc', 'coder_upgrade', 'conversions/coder_upgrade.function');

  if (!$operation) {
    $case = &$case_node->data;
    if (!($case instanceof PGPCase)) {
      cdp("Houston, we've got an unexpected statement");
      return;
    }
    $operation = $case->case->toString();
    $operation = trim($operation, "'\"");
  }
  cdp($operation, '$operation');

  switch ($operation) {
    case 'view':
      /*
       * We want to modify this block. Send it through body_return() with a
       * new callback. The new callback will review the array items at particular
       * keys of interest, e.g. subject and content.
       *
       * To avoid a second layer of callbacks, the first one needs to know the
       * $op values of interest.
       */
      $callback = 'secure_code_review_callback_block_2';
      secure_code_review_traverse_return($case_node->data->body, 'block', $callback, '', 1, 1);
      break;
  }
}

/**
 * Reviews hook_block() array values.
 *
 * @param PGPNode $node
 *   The node of the statement containing the array object.
 * @param PGPArray $array
 *   The array object containing the array element ($current).
 * @param PGPNode $current
 *   The node object of the array element.
 * @param string $hook
 *   The hook name.
 * @param string $type
 *   The type (key or value) of the array element.
 * @param string $key
 *   The key of the array element (or the most recent key).
 * @param string $value
 *   The value of the array element (or NULL if element is a key).
 */
function secure_code_review_callback_block_2($node, &$array, &$current, $hook, $type, $key, $value) { // NEEDS WORK
  cdp("inside " . __FUNCTION__);

  if (!($current instanceof PGPNode)) {
    clp("ERROR: current is not a PGPNode object in hook_$hook");
    return;
  }

  cdp($node->data->toString(), '$node');
  cdp($current->data->toString(), '$current->data');
  cdp($type, '$type');
  cdp($key, '$key');
  cdp($value, '$value');

  $editor = PGPEditor::getInstance();

  /*
   *   - find an array item with key = 'subject'
   *   - inspect the value for check_plain() or t() as with drupal_set_title()
   *   - find an array item with key = 'content'
   *   - if a function call, then evaluate the form code
   */

  if ($type == 'value' && $key == 'subject') {
    cdp("Found the subject key");
    secure_code_review_secure_text_review($node, $current->data, 'In the "subject" array item');
  }
  elseif ($type == 'value' && $key == 'content') {
    cdp("Found the content key");
  }
}

/**
 * Implements hook_upgrade_hook_menu_alter().
 *
 * Values for $op
 * 'list': A list of all blocks defined by the module.
 * 'configure': Configuration form for the block.
 * - need to check form items
 * 'save': Save the configuration options.
 * 'view': Process the block when enabled in a region in order to view its contents.
 * - the 'subject' element is not safe
 */
function secure_code_review_upgrade_hook_menu_alter(&$node, &$reader) {
  clp("\n" . xprintf($node->line, 'Reviewing ' . $node->data->name));
  secure_code_review_traverse_return($node->data->body, 'menu', '', 1, 1);
}

/**
 * Reviews hook_menu arrays.
 *
 * @param PGPNode $node
 *   The node of the statement containing the array object.
 * @param PGPArray $array
 *   The array object containing the array element ($current).
 * @param PGPNode $current
 *   The node object of the array element.
 * @param string $hook
 *   The hook name.
 * @param string $type
 *   The type (key or value) of the array element.
 * @param string $key
 *   The key of the array element (or the most recent key).
 * @param string $value
 *   The value of the array element (or NULL if element is a key).
 */
function secure_code_review_callback_menu($node, &$array, &$current, $hook, $type, $key, $value) { // NEEDS WORK
//   clp(xprintf($node->line, 'Reviewing ' . $type . ' = ' . ($type == 'key' ? $key : $value) . ' (line ' . $node->line . ')'));
  cdp("inside " . __FUNCTION__);

  if (!($current instanceof PGPNode)) {
    clp("ERROR: current is not a PGPNode object in hook_$hook");
    return;
  }

  cdp($node->data->toString(), '$node');
  cdp($current->data->toString(), '$current->data');
  cdp($type, '$type');
  cdp($key, '$key');
  cdp($value, '$value');
//  return;

  $found = FALSE;
  // The keys of this array are the action function items.
  if ($type == 'value' && $key == 'access callback') {
    clp(xprintf($node->line, 'Reviewing type = ' . $type . ' with key = ' . $value));
    cdp("Found the access callback key");
    // Function names are T_CONSTANT_ENCAPSED_STRING and TRUE or FALSE are T_STRING values.

    if ($current->data->isType(T_VARIABLE)) {
      // Parameter is a variable.
      $variable = $current->data->findNode('operand')->toString();
      // Search for an assignment statement to this variable.
      $statement = $node->container->searchBackward('PGPAssignment', 'values', 0, $variable, $node, TRUE);
      if ($statement) {
        $callback = $statement->data->getValue();
        secure_code_review_access_callback_review($statement, $callback, 'On this variable used in the menu item below');
        $found = TRUE;
      }
    }
    else {
      secure_code_review_access_callback_review($node, $current->data);
      $found = TRUE;
    }

    if (!$found) {
      scrlp($node->line,  ' WARNING: Could not find an access callback string to review in hook_menu(). Please confirm it is being set securely.');
    }
  }
}

/**
 * Reviews an access callback expression for safe practice.
 *
 * @param PGPNode $statement
 *   The statement containing the expression to review.
 * @param PGPExpression $expression
 *   The expression to review.
 * @param string $msg_context
 *   The context string to include in the log messages.
 */
function secure_code_review_access_callback_review($statement, $expression, $msg_context = '') { // NEEDS WORK
  cdp(__FUNCTION__);

  if (!is_object($expression)) {
    return;
  }

  // Function names are T_CONSTANT_ENCAPSED_STRING and TRUE or FALSE are T_STRING values.
  if (!$expression->isType(T_CONSTANT_ENCAPSED_STRING) && !$expression->isType(T_STRING)) {
    scrlp($statement->line,  'WARNING:' . $msg_context . ' The value for the "access callback" should usually be a string which is the the name of the function. It may also be assigned the value TRUE or FALSE if the page is always (or never) accessible.');
    return;
  }
  elseif ($expression->isType(T_CONSTANT_ENCAPSED_STRING)) {
    scrlp($statement->line,  'The value for the "access callback" is a string which is (hopefully) the the name of the function.', SECURE_CODE_REVIEW_PASS);
  }
  elseif ($expression->isType(T_STRING)) {
    scrlp($statement->line,  'The value for the "access callback" is the value TRUE or FALSE if the page is always (or never) accessible.', SECURE_CODE_REVIEW_PASS);
  }
}
